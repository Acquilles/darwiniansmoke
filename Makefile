all:

dox: creerdoc
run: smoke

creerdoc:
	doxygen public/dox/Doxyfile
smoke:
	./src/smoke
