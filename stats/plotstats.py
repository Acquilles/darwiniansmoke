# IMPORT
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# FONCTIONS
def lissage(k,v):
    l = []
    n = 2*k+1
    for i in range(k):
        l.append(np.mean(v[:(i+k)]))
    for i in range(k,len(v)-k):
        l.append(np.mean(v[(i-k):(i+k)]))
    for i in range(len(v)-k,len(v)):
        l.append(np.mean(v[(len(v)-k):]))
    return l

def lissage_df(df,k):
    df_lisse = pd.DataFrame(index = df.index)
    for c in df.columns:
        df_lisse[c] = lissage(k, df[c])
    return df_lisse

# LECTURE DES DATAFRAMES

df = pd.read_csv("stats.csv")
print(df.head())

colonnes = list(df.columns)
colonnes.remove('id')
colonnes.remove('time')
colonnes.remove('step')
nb_attr = len(colonnes)

dfg = df.groupby("step").agg(np.mean)
print(dfg.head())
dflisse = lissage_df(dfg,30)

# Affichage des attributs en fonction du temps
fig,axes = plt.subplots(2,1);
p = dflisse
p.plot(y = ["time"], figsize = (20,10),ax = axes[0],title = "Temps de survie moyen au cours du temps")
p.plot(y = colonnes, figsize = (20,10),title = "Attributs moyens au cours du temps",ax = axes[1])

# Affichage des attributs en fonction de l'age
plt.figure(1)
fig,axes = plt.subplots(1,1);
pd.DataFrame(df.groupby("id").apply(max).sort_values("time").set_index("time")).plot(y = colonnes,figsize=(15,7),ax=axes,title = "Évolution des attributs en fonction du temps de survie")

# Affichage de chaque attribut en fonction du temps
plt.figure(2)
p = dflisse
nbc = 2
fig,axes = plt.subplots(nb_attr // nbc +nb_attr%nbc,nbc);
for i in range(nb_attr) :
    p.plot(y = [colonnes[i]], figsize = (20,10),title = '{} moyens au cours du temps'.format(colonnes[i]),ax = axes[i//nbc,i%nbc])
fig.suptitle('Évolution des attributs en fonction du temps', fontsize=16)

# Affichage de la matrice
plt.figure(3)
min_vecu = 50
d = len(colonnes)
fig,axes = plt.subplots(d,d);
for i in range(d):
    for j in range(d):
        df[(df["time"] > min_vecu) ].plot.scatter(x = colonnes[j], y = colonnes[i],ax=axes[i,j],figsize = (20,10),title=colonnes[j])
fig.suptitle('Matrice de correspondances des attributs des vivantes ayant vécu au moins {} steps'.format(min_vecu), fontsize=16)

plt.show()
