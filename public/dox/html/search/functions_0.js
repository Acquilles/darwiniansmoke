var searchData=
[
  ['_5fdisc',['_disc',['../class_screen.html#a39be2e525ff534fc29a76bb4966b5c96',1,'Screen']]],
  ['_5fline',['_line',['../class_screen.html#a1668592e36128bae1e6bf6b24c551134',1,'Screen']]],
  ['_5fput_5fpixel',['_put_pixel',['../class_screen.html#ab202f8be110661884a46fb6c874a46fe',1,'Screen::_put_pixel(SDL_Surface *surf, Uint32 color, int x, int y)'],['../class_screen.html#a7c4fd685536d7777bd01b162d723a53a',1,'Screen::_put_pixel(SDL_Surface *surf, int x, int y, Uint8 r, Uint8 g, Uint8 b)']]],
  ['_5frect',['_rect',['../class_screen.html#a5f324d6925798f20020dbca4f36f37b2',1,'Screen']]],
  ['_5fshow_5fcase',['_show_case',['../class_screen.html#a1ece8706508eb2a75d8ec5a6b4674109',1,'Screen']]]
];
