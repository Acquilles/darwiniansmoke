#ifndef ESQUIVE_H
#define ESQUIVE_H

/** \file Esquive.h
 * Décrit la classe fumée esquive et la classe Esquive_vie*/

#include "../general/Vivante.h"

 //<<<<<<<<<<<<<<<<<<<< Fumee_esquive >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

 /** \class Fumee_esquive
  * \brief  Une fumée Vivante qui meurt si elle rencontre une Entite Hostile du Jeu,
  * et dont le comportement est conçu pour les fuir */
 class Fumee_esquive : public Vivante
 {
 protected:
 //<<<<<<<<<<<<<<<<<<<< Attributs >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     /// La taille minimale autorisée (ne peut pas rétrécir en dessous)
     size_t t_min;
     /// La taille maximale autorisée (ne peut pas grandir au dessus)
     size_t t_max;
     /// Nombre de cases à ajouter chaque step
     size_t NB_ADD;
     /// Nombre de cases à retirer chaque step
     size_t NB_RM;
     /// De quel pourcentage muter (Non opérationnel dans cette version)
     size_t mutateur;

 public:
 //<<<<<<<<<<<<<<<<<<<< Constructeurs >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     /// Construit tous ses attributs de manière aléatoire (bornes arbitraires)
     Fumee_esquive();
     /// Constructeur prenant dans l'ordre : t_min,t_max,NB_ADD,NB_RM,mutateur
     Fumee_esquive(size_t t_min,size_t t_max,size_t NB_ADD,size_t NB_RM,size_t mutateur):Vivante(),t_min(t_min),t_max(t_max),NB_ADD(NB_ADD),NB_RM(NB_RM),mutateur(mutateur){}

 //<<<<<<<<<<<<<<<<<<<< Méthodes publiques (de Vivante et Entite)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     /// Avance d'un pas, appelle la fonction step2
     void step(Map&ext,const std::map<int,Entite*>& corr);
     /// Le type d'une Fumee_esquive est "ESQUIVE" \return "ESQUIVE"
     std::string type() const {return "ESQUIVE";}
     /// Si on est assez loin des projectiles, on a plus de chance de se reproduire
     bool should_greed(const Map&ext, const std::map<int,Entite*>& corr) const;
     /// Crée 1 enfant avec une position aléatoire et des attributs mutés
     std::list<Vivante*> get_children(Map&ext, const std::map<int,Entite*>& corr) const;
     /// Écrit ses caractéristiques (attributs) dans un flux de fichier
     virtual void write_stats(std::ofstream&os)const {os <<_id<<","<<time<<','<<nb_cases()<<","<<t_min<<","<<t_max<<','<<NB_ADD<<","<<NB_RM;}

 protected:
 //<<<<<<<<<<<<<<<<<<<< Méthodes protégées >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     /// Mute un truc
     size_t muter(size_t arg)const;
     /// Mute un vector
     std::vector<size_t> muter(std::vector<size_t>)const;
     /// Etape d'ajout des cases du step
     void step_add(Map&ext,std::map<int,Entite*> corr,const std::list<Entite*>& projectiles);
     /// Etape de suppression des cases du step
     void step_remove(Map&ext,std::map<int,Entite*> corr,const std::list<Entite*>& projectiles);
     /// S'affiche en variation de violet à turquoise
     void show(Screen s);
 };

#endif
