#include "Mystherbe.h"
//<<<<<<<<<<<<<<<<<<<< Constructeurs >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

Mystherbe::Mystherbe():Vivante(),t_min(20),t_max(50),coef(0.1)
{
    /// - Par défaut, tous les attributs sont compris entre -1 et 1
    float binf(-1.0),bsup(1.0);
    cars = std::vector<float>(5);
    for (size_t i = 0; i < 5; i++) {
        cars[i] = ale<float>(binf,bsup);
    }
}

//<<<<<<<<<<<<<<<<<<<< Méthodes publiques (de Vivante et step)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

void Mystherbe::step(Map&ext,const std::map<int,Entite*>& corr)
{
    /// - Incrémente le temps
    time++;
    std::cout <<"Mystherbe"<<_id<<"("<<time<<"): taille "<<nb_cases()<<" 0: "<<cars[0]<<" 1: "<<cars[1]<<" 2: "<<cars[2]<<"3: "<<cars[3]<<" 4: " << cars[4] << std::endl;

    for(const auto& i : cars)
    {
        std::cout << i << ' ';
    }
    std::cout << std::endl;
    /// - Crée la liste des projectiles
    std::list<Entite*> projectiles = entites_hostiles(corr);
    /// - Teste si la fumée doit mourrir (collision avec un Projectile)
    if(touch(this,projectiles))
    {
       clear_it(ext);
        return;
    }
    /// - Lance step_add et step_remove
    step_add(ext,corr,projectiles);
    step_remove(ext,corr,projectiles);
}

bool Mystherbe::should_greed(const Map&ext, const std::map<int,Entite*>& corr) const
{
    /// - La probabilité de se reproduire :
    /// - Augmente avec la distance aux projectiles et la taille de la Fumée
    /// - Décroit avec le nombre de fumées déjà présentes dans le jeu
    return chance(/*size_t(dist_min(this,"PROJ",corr))**/time,100*entites_of_type("MYSTHERBE",corr).size());
}

std::list<Vivante*> Mystherbe::get_children(Map&ext, const std::map<int,Entite*>& corr) const
{
    std::list<Vivante*> children;
    /// - Les attributs de l'enfant proviennent d'une mutation de la mère
    Mystherbe * m = new Mystherbe(muter(cars), t_min, t_max, coef);
    /// - On ajoute l'enfant sur la Map extérieure
    m->add_case(random_form(t_max,ext.random_inside(3)),ext);
    children.push_back(m);
    return children;
}

//<<<<<<<<<<<<<<<<<<<< Fumee_esquive >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

//<<<<<<<<<<<<<<<<<<<< Méthodes protégées >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
float Mystherbe::muter(float f)const
{
    /// - Une chance sur 5 de muter de moitié, sinon de 1 seulement
    if(std::rand() % 5)
    {
        return f + 5*ale<float>(-coef,coef);
    }
    return f + ale<float>(-coef,coef);
}

std::vector<float> Mystherbe::muter(std::vector<float> v)const
{
    std::vector<float> vm(v.size());
    for (size_t i = 0; i < v.size(); i++) {
        vm[i] = muter(v[i]);
    }
    return vm;
}

void Mystherbe::step_add(Map&ext,std::map<int,Entite*> corr,const std::list<Entite*>& projectiles)
{
    /// Ajouter les meileures cases
    std::list<Case> to_add;
    /// - On  associe chaque case de la brodure extérieure avec sa distance avec le projectile le plus proche
    std::vector<std::pair<float,Case>> distance_indice_ext;
    for(const Case&c : map.get_ext_bounds())
    {
        distance_indice_ext.push_back(std::make_pair<float,Case>(dist_min(c,projectiles),Case(c)));
    }
    /// - On  trie cette table
    std::sort(distance_indice_ext.begin(), distance_indice_ext.end());

    /// - On détermine le nombre de cases à ajouter (en fonction de l'écart avec la taille maximale)
    float nba = cars[0]*t_max + cars[1]*t_min + cars[2]*nb_cases() + cars[3]*distance_indice_ext.front().first;
    if(nba<0.0) nba = 0.0;
    size_t nb_to_add = size_t(nba);

    /// - On ajoute les meileures cases
    size_t i=0;
    for(const auto& p : distance_indice_ext)
    {
        if(nb_to_add + i++ >= distance_indice_ext.size())
        {
            to_add.push_back(p.second);
        }
    }
    add_case(to_add,ext);

}
void Mystherbe::step_remove(Map&ext,std::map<int,Entite*> corr,const std::list<Entite*>& projectiles)
{
    /// Retirer les pires cases
    std::list<Case> to_remove;
    /// - On  associe chaque case de la brodure intérieure avec sa distance avec le projectile le plus proche
    std::vector<std::pair<float,Case>> distance_indice_in;
    for(const Case&c : map.get_in_bounds())
    {
        distance_indice_in.push_back(std::make_pair<float,Case>(dist_min(c,projectiles),Case(c)));
    }
    /// - On  trie cette table
    std::sort(distance_indice_in.begin(), distance_indice_in.end());

    /// - On détermine le nombre de cases à retirer (en fonction de l'écart avec la taille minimale)
    size_t i=0;
    float nbr = cars[1]*t_max + cars[0]*t_min - cars[2]*nb_cases() + cars[4]*distance_indice_in.back().first;
    if(nbr<0.0) nbr = 0.0;
    size_t nb_to_rm = size_t(nbr);

    /// - On retire les pires cases
    i=0;
    for(const auto& p : distance_indice_in)
    {
        if(i++ < nb_to_rm)
        {
            to_remove.push_back(p.second);
        }
    }
    remove_case(to_remove,ext);
}

void Mystherbe::write_stats(std::ofstream&os)const {
    os << _id << ',' << time;
    for(const auto& i : cars)
    {
        os <<','<< i;
    }
    os << std::endl;
}

void Mystherbe::show(Screen s){
    s.show_case(cases,1.0,0x02F42B +((47*_id)%256)*256);
}
