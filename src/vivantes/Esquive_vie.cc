#include "Esquive_vie.h"

void Esquive_vie::step(Map&ext,const std::map<int,Entite*>& corr)
{
    /// - Incrémente le temps
    time++;
    std::cout <<"V:"<<_id<<"("<<time<<"): taille "<<nb_cases()<<", min "<<t_min<<", max "<<t_max<<", NB_ADD "<<NB_ADD<<", NB_RM "<<NB_RM<<std::endl;
    /// - Crée la liste des projectiles
    std::list<Entite*> projectiles = entites_hostiles(corr);
    /// - Teste si la fumée doit retirer un pojnt de vie (collision avec un Projectile)
    if(touch(this,projectiles))
    {
       pdv = pdv -1;
    }
    /// - Lance step_add et step_remove
    step_add(ext,corr,projectiles);
    step_remove(ext,corr,projectiles);
}

std::list<Vivante*> Esquive_vie::get_children(Map&ext, const std::map<int,Entite*>& corr) const
{
    std::list<Vivante*> children;
    /// - Les attributs de l'enfant proviennent d'une mutation de la mère
    Esquive_vie * f = new Esquive_vie(muter(t_min),
                                        muter(t_max),
                                        muter(NB_ADD),
                                        muter(NB_RM),
                                        muter(mutateur),
                                        muter(pdv));
    /// - On ajoute l'enfant sur la Map extérieure
    f->add_case(random_form(f->t_min,ext.random_inside(3)),ext);
    children.push_back(f);
    return children;
}

void Esquive_vie::show(Screen s){
    s.show_case(cases,1.0,0x02F42B +((47*_id)%256)*256);
}
