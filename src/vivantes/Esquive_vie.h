#ifndef ESQVIE_H
#define ESQVIE_H

#include "Fumee_esquive.h"
/** \file Esquive_vie.h
 * Décrit la classe Esquive_vie*/

/** \class Esquive_vie
* Une Fumée esquive avec des points de vie, retirés à chaque rencontre d'une entité hostile*/
 class Esquive_vie : public Fumee_esquive
 {
 protected:
     /// Les points de vies
     int pdv;
 public:
     /// Construit tous ses attributs de manière aléatoire (bornes arbitraires)
     Esquive_vie():Fumee_esquive(){}
     /// Construit tous ses attributs
     Esquive_vie(size_t t_min,size_t t_max,size_t NB_ADD,size_t NB_RM,size_t mutateur,size_t pdv):Fumee_esquive(t_min, t_max, NB_ADD, NB_RM, mutateur),pdv(pdv){}
     /// Elle meurt lorsqu'elle n'a pas de points de vie
     bool active(){return pdv > 0;}
     /// Crée 1 enfant avec une position aléatoire et des attributs mutés
     std::list<Vivante*> get_children(Map&ext, const std::map<int,Entite*>& corr) const;
     /// Avance d'un pas, appelle la fonction step2
     void step(Map&ext,const std::map<int,Entite*>& corr);
     /// S'affiche en nuance de vert à jaune
     void show(Screen s);
 };

 #endif
