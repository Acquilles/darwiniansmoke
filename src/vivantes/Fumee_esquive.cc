#include "Fumee_esquive.h"
//<<<<<<<<<<<<<<<<<<<< Constructeurs >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

/// Un Constructeur par défaut avec des valeurs arbitraires, non utilisé dans les simulation
Fumee_esquive::Fumee_esquive():Fumee_esquive(ale<size_t>(15,30),ale<size_t>(30,50),ale<size_t>(10,15),ale<size_t>(10,15),2){}

//<<<<<<<<<<<<<<<<<<<< Méthodes publiques (de Vivante et step)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

void Fumee_esquive::step(Map&ext,const std::map<int,Entite*>& corr)
{
    /// - Incrémente le temps
    time++;
    std::cout <<"E"<<_id<<"("<<time<<"): taille "<<nb_cases()<<", min "<<t_min<<", max "<<t_max<<", NB_ADD "<<NB_ADD<<", NB_RM "<<NB_RM<<std::endl;
    /// - Crée la liste des projectiles
    std::list<Entite*> projectiles = entites_hostiles(corr);
    /// - Teste si la fumée doit mourrir (collision avec un Projectile)
    if(touch(this,projectiles))
    {
       clear_it(ext);
        return;
    }
    /// - Lance step_add et step_remove
    step_add(ext,corr,projectiles);
    step_remove(ext,corr,projectiles);
}

bool Fumee_esquive::should_greed(const Map&ext, const std::map<int,Entite*>& corr) const
{
    /// - La probabilité de se reproduire :
    /// - Augmente avec la distance aux projectiles et la taille de la Fumée
    /// - Décroit avec le nombre de fumées déjà présentes dans le jeu
    return chance(size_t(dist_min(this,"PROJ",corr))*time,8000*entites_of_type("ESQUIVE",corr).size());
}

std::list<Vivante*> Fumee_esquive::get_children(Map&ext, const std::map<int,Entite*>& corr) const
{
    std::list<Vivante*> children;
    /// - Les attributs de l'enfant proviennent d'une mutation de la mère
    Fumee_esquive * f = new Fumee_esquive(muter(t_min),
                                        muter(t_max),
                                        muter(NB_ADD),
                                        muter(NB_RM),
                                        muter(mutateur));
    /// - On ajoute l'enfant sur la Map extérieure
    f->add_case(random_form(f->t_min,ext.random_inside(3)),ext);
    children.push_back(f);
    return children;
}

//<<<<<<<<<<<<<<<<<<<< Fumee_esquive >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

//<<<<<<<<<<<<<<<<<<<< Méthodes protégées >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
size_t Fumee_esquive::muter(size_t t)const
{
    float perf = float(time);
    float n = float(normal(float(t),10.0*t/perf));
    //std::cout << t << " -> " << n <<" = "<< round(n)*((n>0)*2-1)<< std::endl;
    return round(n)*((n>0)*2-1);
}

void Fumee_esquive::step_add(Map&ext,std::map<int,Entite*> corr,const std::list<Entite*>& projectiles)
{
    /// Ajouter les meileures cases
    std::list<Case> to_add;
    /// - On  associe chaque case de la brodure extérieure avec sa distance avec le projectile le plus proche
    std::vector<std::pair<float,Case>> distance_indice_ext;
    for(const Case&c : map.get_ext_bounds())
    {
        distance_indice_ext.push_back(std::make_pair<float,Case>(dist_min(c,projectiles),Case(c)));
    }
    /// - On  trie cette table
    std::sort(distance_indice_ext.begin(), distance_indice_ext.end());

    /// - On détermine le nombre de cases à ajouter (en fonction de l'écart avec la taille maximale)
    size_t i=0;
    size_t nb_to_add= NB_ADD;
    if(nb_cases() > t_max)
    {
        nb_to_add = 0;
    }
    else if( NB_ADD  + nb_cases() > t_max )
    {
        nb_to_add = t_max - nb_cases();
    }

    /// - On ajoute les meileures cases
    for(const auto& p : distance_indice_ext)
    {
        if(nb_to_add + i++ >= distance_indice_ext.size())
        {
            to_add.push_back(p.second);
        }
    }
    add_case(to_add,ext);
}

void Fumee_esquive::step_remove(Map&ext,std::map<int,Entite*> corr,const std::list<Entite*>& projectiles)
{
    /// Retirer les pires cases
    std::list<Case> to_remove;
    /// - On  associe chaque case de la brodure intérieure avec sa distance avec le projectile le plus proche
    std::vector<std::pair<float,Case>> distance_indice_in;
    for(const Case&c : map.get_in_bounds())
    {
        distance_indice_in.push_back(std::make_pair<float,Case>(dist_min(c,projectiles),Case(c)));
    }
    /// - On  trie cette table
    std::sort(distance_indice_in.begin(), distance_indice_in.end());

    /// - On détermine le nombre de cases à retirer (en fonction de l'écart avec la taille minimale)
    size_t i=0;
    size_t nb_to_rm = NB_RM;
    if(nb_cases() < t_min)
    {
        nb_to_rm = 0;
    }
    else if(nb_cases() < t_min+NB_RM )
    {
        nb_to_rm = nb_cases() - t_min;
    }

    /// - On retire les pires cases
    for(const auto& p : distance_indice_in)
    {
        if(i++ < nb_to_rm)
        {
            to_remove.push_back(p.second);
        }
    }
    remove_case(to_remove,ext);
}

std::vector<size_t> Fumee_esquive::muter(std::vector<size_t> v)const
{
    std::vector<size_t> vm(v.size());
    for (size_t i = 0; i < v.size(); i++) {
        vm[i] = muter(v[i]);
    }
    return vm;
}

void Fumee_esquive::show(Screen s){
    s.show_case(cases,1.0,0x9BCCFF +((13*_id)%96)*256);
}
