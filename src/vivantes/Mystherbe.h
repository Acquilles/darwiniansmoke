#ifndef MYSTHERBE_H
#define MYSTHERBE_H

/** \file Esquive.h
 * Décrit la classe Mystherbe */

#include "../general/Vivante.h"

 //<<<<<<<<<<<<<<<<<<<< Mystherbe >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

 /** \class Mystherbe
  * \brief  Une fumée Vivante qui meurt si elle rencontre une Entite hostile du Jeu,
  * et dont le comportement est conçu pour les fuir.
  * Elle difère des Fumée_esquives par sa façon de calculer le nombre de cases qu'elle doit ajouter et retirer chaque tour */

 class Mystherbe : public Vivante
 {
 protected:
 //<<<<<<<<<<<<<<<<<<<< Attributs >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     /// Liste des caractéristiques
    std::vector<float> cars;
    /// Taille minimale
    size_t t_min ;
    /// Taille maximale
    size_t t_max ;
    /// Coefficient de mutation
    float coef;

 public:
 //<<<<<<<<<<<<<<<<<<<< Constructeurs >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     /// Construit tous ses attributs de manière aléatoire (bornes arbitraires)
     Mystherbe();
     /// Constructeur prenant tous les attributs, les cars étant de taille 5
     Mystherbe(std::vector<float> cars,size_t t_min, size_t t_max, float coef):Vivante(),cars(cars),t_min(t_min),t_max(t_max),coef(coef){}

 //<<<<<<<<<<<<<<<<<<<< Méthodes publiques (de Vivante et step)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     /// Avance d'un pas, appelle la fonction step2
     void step(Map&ext,const std::map<int,Entite*>& corr);
     /// Le type d'une Mystherbe est "MYSTHERBE" \return "MYSTHERBE"
     std::string type() const {return "MYSTHERBE";}
     /// Si on est assez loin des projectiles, on a plus de chance de se reproduire
     bool should_greed(const Map&ext, const std::map<int,Entite*>& corr) const;
     /// Crée 1 enfant avec une position et une forme aléatoire et des attributs mutés
     std::list<Vivante*> get_children(Map&ext, const std::map<int,Entite*>& corr) const;
     /// Écrit ses caractéristiques (attributs) dans un flux de fichier
     virtual void write_stats(std::ofstream&os)const ;
     /// S'affiche en nuance de vert à jaune
     void show(Screen s);

 protected:
 //<<<<<<<<<<<<<<<<<<<< Méthodes protégées >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     /// Mute un truc
     float muter(float arg)const;
     /// Mute un vector
     std::vector<float> muter(std::vector<float>)const;
     /// Étape d'ajout des cases de step
     void step_add(Map&ext,std::map<int,Entite*> corr,const std::list<Entite*>& projectiles);
     /// Étape de suppression des cases de step
     void step_remove(Map&ext,std::map<int,Entite*> corr,const std::list<Entite*>& projectiles);
 };
#endif
