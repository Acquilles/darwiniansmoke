#include "Mur.h"

Mur::Mur(int x_min,int x_max,int y_min,int y_max, Map&m):Entite()
{
    std::list<Case> to_add;
    for (int i = x_min; i <= x_max; i++)
    {
        to_add.push_back(Case(i,y_min));
        to_add.push_back(Case(i,y_max));
    }
    for (int i = y_min; i <= y_max; i++)
    {
        to_add.push_back(Case(x_min,i));
        to_add.push_back(Case(x_max,i));
    }
    add_case(to_add,map);
}
