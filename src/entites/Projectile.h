#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "../general/Entite.h"

/** \file Projectile.h
 * Décrit la classe Projectile */

//<<<<<<<<<<<<<<<<<<<< Projectile >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/// Laps de temps d'un pas d'un Projectile (règle leur vitesse)
#define DT 0.7

/** \class Projectile
 * \brief Une Entite de forme constante et de trajectoire rectiligne,
 * qui rebondit sur des bords fixés à l'initialisation.*/

class Projectile : public Entite
{
protected:
    /// Coordonnée flottante x
    float x;
    /// Coordonnée flottante y
    float y;
    /// Vitesse flottante x
    float vx;
    /// Vitesse flottante y
    float vy;
    /// Coordonées des bords sur lesquels le Projectile rebondit
    float x_min;
    /// Coordonées des bords sur lesquels le Projectile rebondit
    float x_max;
    /// Coordonées des bords sur lesquels le Projectile rebondit
    float y_min;
    /// Coordonées des bords sur lesquels le Projectile rebondit
    float y_max;
    /// Coordonnée x entière, sert à placer le Projectile sur les bonnes cases
    int cx;
    /// Coordonnée x entière, sert à placer le Projectile sur les bonnes cases
    int cy;
    /// Met à jour ses coordonnées flottantes dans son déplacement
    void update();
    /// Le type d'un Projectile est "PROJ" \return "PROJ"
    virtual std::string type() const {return "PROJ";}
    /// Un Projectile est une Entite hostile \return true
    virtual bool hostile()const {return true;}
public:
    /// Initialise le Projectile avec son centre \p pos, ses vitesses et ses bords
    Projectile(Case pos,float vx,float vy,Case min, Case max):Entite(),x(pos.x),y(pos.y),vx(vx),vy(vy),x_min(min.x),x_max(max.x),y_min(min.y),y_max(max.y),cx(pos.x),cy(pos.y){}
    /// Appelle update et fait déplacer le Projectile
    void step(Map&ext,const std::map<int,Entite*>& corr);
    /// S'affiche en petites boules rouges sur l'écran
    void show(Screen s){s.show_case(cases,0.60,0xFF0000);}
};

#endif
