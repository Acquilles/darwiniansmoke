#ifndef MUR_H
#define MUR_H

#include "../general/Entite.h"
/** \class Mur
 * \brief Entite qui entoure une carte, ne bouge pas et est hostile */
class Mur : public Entite
{
public:
    /// 4 murs entourant ces coordonnées
    Mur(int x_min,int x_max,int y_min,int y_max, Map&m);
    /// Est hostile
    virtual bool hostile() const {return true;}
    /// Ne bouge pas
    virtual void step(Map&m, const std::map<int,Entite*>& corr) {}
    /// Se montre en noir
    virtual void show(Screen s){s.show_case(cases,1.0,0x000000);}
};
 #endif
