#ifndef FUMEE_TEST_H
#define FUMEE_TEST_H

#include "../general/Vivante.h"


#define T_MIN  50
#define T_MAX  100
/*!
 * \class Fumee_test
 * Une fumée Vivante qui, pour se déplacer (voir fonction step), utilise deux Case repère extérieures.
 * C'est good, vers laquelle elle est censé se diriger,et evil, qu'elle est censé fuir.
 * Pour cela, uà chaque pas, on parcours la frontière extérieure de la fumée, et une case a
 * plus de chance d'y apparaître si elle est loin de evil. Puis, on parcours la frontière
 * intérieure et une Case a plus de chace d'y disparaître si elle est loin de la Case
 * evil.
 * \brief Une première tentative de fumée Vivante, qui nous sert à tester des comportements acceptables
 */
class Fumee_test : public Entite
{
protected:
    /// Taille minimale
    size_t t_min;
    /// Taille maximale
    size_t t_max;

    /// Case qu'il faut fuir
    Case evil;
    /// Case vers laquelle il faut aller
    Case good;
    /// Temps de changement aléatoire de good
    size_t GOOD_TIME;
    /// Temps de changement aléatoire de evil
    size_t EVIL_TIME;

public:
//<<<<<<<<<<<<<<<<<<<< ENTITE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    Fumee_test(Map&m,size_t g, size_t e,size_t min, size_t max):Entite(),t_min(min),t_max(max),evil(m.random_bordure()),good(m.random_bordure()),GOOD_TIME(g),EVIL_TIME(e){}
    void step(Map&m, const std::map<int,Entite*>& corr);

//<<<<<<<<<<<<<<<<<<<< FUMEE_TEST >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Affiche les cases good et evil en petits cercles vert et rouge
    void print_moral(Screen s)
    {
        s.show_case(good,0.3,0x00FF00);
        s.show_case(evil,0.3,0xFF0000);
    }
    /// Affiche la fumée en nuance de vert et affiche aussi sa morale
    virtual void show(Screen s){s.show_case(cases,1.2,0x222222 + (256*(256+1)+1)*((6*_id)%256));print_moral(s);}
    /*!
     *  Indique si la case (extérieure) doit appartenir à la fumée
     */
    bool will_born(const Case&c);
    /*!
     *  Indique si la case (intérieure) doit disparaître de la fumée
     */
    bool will_die(const Case&c);
    /*!
     *  Change de place good et evil aléatoirement dans la map
     */
    void change_moral(const Map&m);
    /// C'est une fumée hostile \return true
    bool hostile()const {return true;}
};

#endif
