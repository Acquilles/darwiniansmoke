#include "Projectile.h"
//<<<<<<<<<<<<<<<<<<<< Projectile >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

void Projectile::update()
{
    /// - Si le centre est en dehors des bords :
    if(x<x_min or y<y_min or x>x_max or y>y_max)
    {
      /// - . Si c'est en dehors de x :
      if(x<x_min or x>x_max)
      {
        /// - .. On remet le centre à la limite et on inverse la vitesse en x
        x = (x>x_max)*(x_max) + y_min*(x<x_min);
        vx = -vx;
      }

      /// - . Si c'est en dehors de y :
      if(y<y_min or y>y_max)
      {
          /// - .. On remet le centre à la limite et on inverse la vitesse en y
        y = (y>y_max)*(y_max) + y_min*(y<y_min);
        vy = -vy;
      }
    }

    /// - On met à jour la position avec la vitesse
    x += vx*DT;
    y += vy*DT;
}

void Projectile::step(Map&ext,const std::map<int,Entite*>& corr)
{
    time++;
    /// - On update les coordonnées flottantes
    update();
    std::list<Case> to_add;
    /// - Si les cooordonnées entières sont différentes du cast des flottantes :
    if((int(x)-cx != 0) or (int(y)-cy !=0))
    {
        /// - . Pour chaque Case de l'Entite, on lui ajoute la différence
        for(const Case& c: cases)
        {
            to_add.push_back(c + Case(int(x)-cx,int(y)-cy));
        }
        /// - . On met à jour le centre entier
        cx =int(x);
        cy = int(y);
        /// - . On remplace l'anciene liste de cases par la nouvelle
        clear_it(ext);
        add_case(to_add,ext);
    }
}
