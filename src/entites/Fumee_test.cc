#include "Fumee_test.h"
/*! \file Fumee_test.cc Définit les fonctions membres de Fumee_test */

//<<<<<<<<<<<<<<<<<<<< MEMBRES ENTITES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

void Fumee_test::step(Map&m, const std::map<int,Entite*>& corr)
{
    /// - Change ou pas les good et evil
    change_moral(m);
    /// - Pour chaque case de la frontière exterieure, si elle doit naître on l'ajoute
    std::list<Case> to_add;
    std::list<Case> to_remove;
    for(const auto& c : map.get_ext_bounds())
    {
        if(will_born(c))
        {
            to_add.push_back(Case(c));
        }
    }
    /// - Pour chaque case de la frontière intérieure, si elle doit mourrir on la retire
    add_case(to_add,m);;
    for(const auto& c : map.get_in_bounds())
    {
        if(will_die(c))
        {
            to_remove.push_back(Case(c));
        }
    }
    remove_case(to_remove,m);
}

//<<<<<<<<<<<<<<<<<<<< MEMBRES FUMEE_TEST >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
bool Fumee_test::will_born(const Case&c)
{
    /// - Si la fumée est trop grande, renvoyer false
    if (cases.size()>t_max)
    {
        return false;
    }
    /// - Si la fumée est sur le evil, renvoyer false
    if(dist(c,evil)==0)
    {
        return false;
    }
    /// - La chance de naître augment avec la distance au evil
    return chance(dist(c,evil),(dist(c,good)+dist(c,evil)));}

bool Fumee_test::will_die(const Case&c)
{
    /// - Si la fumée est trop petite, renvoyer false
    if (cases.size()<t_min)
    {
        return false;
    }
    /// - Si la fumée est sur le good, renvoyer false
    if(dist(c,good)==0)
    {
        return true;
    }
    /// - La chance de mourrir augment avec la distance au good

    return chance(dist(c,good),(dist(c,good)+dist(c,evil)));
}


void Fumee_test::change_moral(const Map& m)
{
    /// - Si c'est le temps de changer good ou evil :
    /// - . On renvoie un nouveau god ou evil aléatoirement à l'interieur de la carte
    time++;
    if((time) % GOOD_TIME == 0)
    {
        good = m.random_inside();
    }
    if((time) % EVIL_TIME == 0)
    {
        evil = m.random_inside();
    }
}
