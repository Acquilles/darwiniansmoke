#include "../general/Jeu.h"
/** \file test.cc
 * \brief Quelques fonctions de test*/

/** \fn test_contigues Teste la fonction contigues*/
void test_contigues()
{
    /// Au sein d'une map, déclare 3 positions différenteset affiche leur contiguité
    Map m(file_to_list_cases("test_contigues.txt",Case(0,0)));
    Case posa(0,0);
    Case posb(2,14);
    Case posc(6,1);
    std::list<Case> la = m.contigues(posa);
    std::list<Case> lb = m.contigues(posb);
    std::list<Case> lc = m.contigues(posc);
    Screen s;
    while(s.flip())
    {
        s.show_case(la,1.0,0x00FF00);
        s.show_case(lb,1.0,0x0000FF);
        s.show_case(lc,1.0,0xFF0000);
        s.axes();
    }
}
