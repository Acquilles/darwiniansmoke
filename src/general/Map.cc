#include "Map.h"
/*! \file Map.cc Quelques fonctions de Map.h*/
//<<<<<<<<<<<<<<<<<<<< CONSTRUCTEURS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Map::Map(const std::list<Case>& liste_cases)
{
    int x_min , y_min, x_max, y_max;
    std::tie(x_min , y_min, x_max, y_max) = get_bounds(liste_cases);
    min= Case(x_min,y_min);
    max= Case(x_max,y_max);
    map = std::vector<std::vector<int>>(x_max-x_min+1,std::vector<int>(y_max-y_min+1,false));

    for(const auto& it : liste_cases){
        map[it.x-x_min][it.y-y_min] = 1;
    }
}

//<<<<<<<<<<<<<<<<<<< ACCESSEURS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
int Map::operator()(int x, int y)const
{
    return map[x-min.x][y-min.y];
}
int  Map::operator()(const Case&c)const
{
    return map[c.x-min.x][c.y-min.y];
}

bool Map::inside(int x, int y)const
{
    return (x>=min.x and y>=min.y and x<=max.x and y<=max.y);
}

int Map::get(int x, int y)const
{
    if(inside(x,y))
    {
        return map[x-min.x][y-min.y];
    }
    return 0;
}

bool Map::set(int x, int y, int val){
    if(inside(x,y))
    {
        map[x-min.x][y-min.y] = val;
        return true;
    }
    return false;
}

//<<<<<<<<<<<<<<<<<<< A UTILISER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
bool Map::add_cases(const std::list<Case>& l,int id)
{
    for(const Case& c : l)
    {
        if(get(c)==0)
        {
            set(c,id);
        }
        else
        {
            //std::cout << "Tentative de superposition en "<< c << " de " << id << " sur " << get(c) << std::endl;
        }
    }
    return true;
}

bool Map::remove_cases(const std::list<Case>& l,int id)
{
    for(const Case& c : l)
    {
        if(get(c)==id)
        {
            set(c,0);
        }
        else
        {
            //std::cout << "Tentative de supression en "<< c << " de " << id << " sur " << get(c) << std::endl;
        }
    }
    return true;
}

Case Map::random_bordure() const
{
    int a = std::rand() % 4;
    if(a>1)
    {
        return Case(ale<int>(min.x,max.x),(a==2)*min.y+(a==3)*max.y);
    }
    return Case((a==0)*min.x+(a==1)*max.x,ale<int>(min.y,max.y));
}

//<<<<<<<<<<<<<<<<<<<<<<< METHODES DE PARCOURS >>>>>>>>>>>>>>>>>>>>>>>>>

int Map::nb_case(int rad, const Case&c)const
{
    int nb(0);
    /// - On parcours les positions de \f$(x_c-rad,y_c-rad)\f$ à \f$(x_c+rad,y_c+rad)\f$
    for (int i = c.x - rad; i <= c.x + rad; i++) {
        for (int j = c.y - rad; j <= c.y + rad; j++) {
            /// - . Si \f$(i,j)\f$ est bien dans le rayon : \f$(x_c-i)^2 + (y_c-i)^2 < rad^2\f$
            if((c.x-i)*(c.x-i) + (c.y-j)*(c.y-j) <= rad*rad)
            {
                /// - .. On ajoute 1 si cette position n'est pas nulle
                nb+=(get(i,j)!=0);
            }
        }
    }
    return nb;
}

bool Map::one_case(int val,const Case&c)const
{
    /// - Pour chaque Case à gauche, droite, haut et bas de \p c :
    for(const auto& a : contour4(c))
    {
        /// - . Si sur cette position on a \p val, renvoyer vrai
        if(get(a) == val){
            return true;
        }
    }
    return false;
}

std::list<Case> Map::get_in_bounds()const
{
    std::list<Case> in_bounds;
    /// - Pour chaque position \f$(i,j)\f$ de la map :
    for(int i =min.x ; i <= max.x; i++){
        for(int j =min.y ; j <= max.y; j++){
            /// - . Si la position n'est pas vide :
            if(get(i,j))
            {
                /// - .. S'il y a au moins une case vide autour, on ajoute cette position
                if(one_case(0,Case(i,j))){
                    in_bounds.push_back(Case(i,j));
                }
            }
        }
    }
    return in_bounds;
}

std::list<Case> Map::get_ext_bounds() const
{
    std::list<Case> ext_bounds;
    /// - Pour chaque position \f$(i,j)\f$ de la map :
    for(int i =min.x-1 ; i <= max.x+1; i++){
        for(int j =min.y-1 ; j <= max.y+1; j++){
            /// - . Si la position est vide ou en dehors de la carte :
            if(!get(i,j))
            {
                /// - .. S'il y a au moins une case pleine autour, on ajoute cette position
                if(one_case(true,Case(i,j))){
                    ext_bounds.push_back(Case(i,j));
                }
            }
        }
    }
    return ext_bounds;
}

std::string Map::string_map()const
{
    std::ostringstream s;
    s << min << " -> " << max <<std::endl;
    for(const auto & itx : map)
    {
        for(const auto & ity : itx)
        {
            if(ity!=0){
                s<<ity<< ' ';
            }
            else
            {
                s<<' '<< ' ';
            }
        }
        s << std::endl;
    }
    return s.str();
}

std::string Map::string_map_plus(const std::list<Case>& plus,char car)const
{
    std::tuple<int, int, int, int> b = get_bounds(plus);
    int x_min(min.x),y_min(min.y),x_max(max.x),y_max(max.y);
    if (plus.size() !=0)
    {
        x_min = (std::min(min.x,std::get<0>(b)));
        y_min = (std::min(min.y,std::get<1>(b)));
        x_max = (std::max(max.x,std::get<2>(b)));
        y_max = (std::max(max.y,std::get<3>(b)));
    }

    std::ostringstream s;
    s << min << " -> " << max <<std::endl;
    std::vector<std::vector<char>> M(x_max-x_min+1,std::vector<char>(y_max-y_min+1, ' '));

    for (int i = min.x; i <= max.x; i++) {
        for (int j = min.y; j <= max.y; j++) {
            if(get(i,j) !=0)
            {
                M[i-x_min][j-y_min] = '*';
            }
        }
    }
    for(const auto & c : plus)
    {
        M[c.x-x_min][c.y-y_min] = car;
    }
    for (int i = 0; i < x_max-x_min+1; i++) {
        for (int j = 0; j < y_max-y_min+1; j++) {
            s << M[i][j]<< ' ';
        }
        s << std::endl;
    }
    return s.str();
}



void Map::contigues(const Case& c, std::set<Case>& toutes)
{
    /// Fonction récursive :
    /// - Si la Case \p c n'est pas déjà dans \p toutes :
    if(toutes.find(c)==toutes.end())
    {
        /// - . On insère \p c à \p toutes, et pour chaque Case \b a autour de \p c :
        toutes.insert(c);
        for(const auto& a : contour8(c))
        {
            /// - .. Si la valeur de la position \b a est la même que celle de la case \p c:
            if(get(a)==get(c))
            {
                /// - ... On éxécute la fonction sur \b a
                contigues(a,toutes);
            }
        }
    }
}

std::list<Case> Map::contigues(const Case&c)
{
    /// Obtient simplement le set des cases grâce à la
    /// fonction contigues précédente et le transforme en liste
    std::list<Case> l;
    std::set<Case> s;
    contigues(c,s);
    for (const Case& a : s)
    {
        l.push_back(a);
    }
    return l;
}

std::ostream& operator<<(std::ostream& os, const std::list<Case>& liste_cases){
    /// Transforme la liste en Map et affiche celle-ci
    os << Map(liste_cases);
    return os;
}

//<<<<<<<<<<<<<<<<<<< EXTERIEURES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

std::list<std::list<Case>> disloque(const std::list<Case>& liste)
{
    std::set<Case> toutes;
    std::list<std::list<Case>> l;
    Map m(liste);
    /// - Pour chaque Case de la liste :
    for(const Case& c : liste)
    {
        /// - . Si elle n'est pas encore dans une des nouvelles listes
        if(toutes.find(c)==toutes.end())
        {
            /// - .. On ajoute la liste de toutes les cases contigues à celle_ci
            l.push_back(std::list<Case>());
            for(const Case&d : m.contigues(c))
            {
                toutes.insert(d);
                l.back().push_back(d);
            }
        }
    }
    return l;
}
