#ifndef CASE_H
#define CASE_H

/*!
 * \file Case.h
 * \brief Définition de la classe Case et de ses fonctions amies
 */

#include "Utility.h"
/*!
 * \class Case
 * \brief La Case est l'objet utilisé pour représenter simplement 2 coordonnées
 */

class Case
{
protected:
//<<<<<<<<<<<<<<<<<<<<<<< ATTRIBUTS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Coordonnée x
    int x;
    /// Coordonnée y
    int y;
public:
//<<<<<<<<<<<<<<<<<<<< CONSTRUCTEURS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Constructeur initialisant les coordonées
    Case(int x=0, int y=0):x(x),y(y){}


//<<<<<<<<<<<<<<<<<<<<<<< OPERATORS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Teste l'égalité des coordonnées
    bool operator==(const Case& c) const{ return (c.x==x and c.y == y);};
    /// Oprateur d'affectation, copie les coordonnées
    void operator=(const Case& c){x=c.x;y=c.y;}
    /// Crée une nouvelle Case avec la somme des coordonnées
    Case operator+(const Case& c)const{return Case(x+c.x,y+c.y);}
    /// Opérateur de comparaison, utilisé par les std::set. Compare x puis y
    friend bool operator<(const Case&a,const Case&c){if(a.x==c.x){return a.y < c.y;}return a.x < c.x;}

//<<<<<<<<<<<<<<<<<<<<<<<<< FRIENDS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Question de pratique, les Map et Screen sont amies
    friend class Map;
    /// Question de pratique, les Map et Screen sont amies
    friend class Screen;
    /// A éviter
    friend class Projectile;
//<<<<<<<<<<<<<<<<<<<<<<< Utilisant une Case >>>>>>>>>>>>>>>>>>>>>>>>
    ///Affiche (x,y)
    friend std::ostream& operator<<(std::ostream& , const Case &);
    /*!
     * \brief indique si la case est comprise entre le min et le max
     * \param c Case à tester
     * \param min Bord minimal
     * \param max Bord maximal
     * \return vrai(true) si c est comprise entre min et max
     */
    friend bool is_in_bounds(const Case& c, const Case&min, const Case&max);
    /*!
     * \brief Calcule la distance entre deux cases. Par défaut, distance euclidienne
     */
    friend int dist(const Case& a, const Case& b){return pow(a.x-b.x,2)+pow(a.y-b.y,2);}
    /*!
     * \brief Renvoie la liste des 8 cases entourant celle en entrée
     */
    friend std::list<Case> contour8(const Case& c);
    /*!
     * \brief Renvoie la liste des 4 cases(gauche, droite, au dessus, en dessous) entourant celle en entrée
     */
    friend std::list<Case> contour4(const Case& c);
//<<<<<<<<<<<<<<<<<<<<<<< Utilisant une liste >>>>>>>>>>>>>>>>>>>>>>>
    /*!
     * \brief Renvoie les extrémités d'une liste de cases
     * \return x_min,y_min,x_max,y_max
     */
    friend std::tuple<int,int,int,int> get_bounds(const std::list<Case>& liste_cases);
    /// /// Lit un fichier et le convertit en map de correpsondances de caractères
    friend std::map<char,std::list<Case>> file_to_map_corr(std::string filename, const Case& min);
    /// Lit un fichier et le convertit en liste de cases correspondant aux *
    friend std::list<Case> file_to_list_cases(std::string filename, const Case& min);
};
//<<<<<<<<<<<<<<<<<<< EXTERIEURES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/// Renvoie une forme contigue aléatoire de taille \p nb et de centre \p c
void random_form_rec(size_t nb, const Case&c, std::set<Case>& toutes);
/// Renvoie une forme contigue aléatoire de taille \p nb et de centre \p c
std::list<Case> random_form(size_t nb, const Case& c);

#endif
