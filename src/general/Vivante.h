#ifndef VIVANTE_H
#define VIVANTE_H

/** \file Vivante.h Définit la classe Vivante */
#include "Entite.h"

/*!
 * \class Vivante
 * \brief Une Entite qui peut se reproduire
 */
class Vivante : public Entite
{
public:
//<<<<<<<<<<<<<<<<<<<< ENTITE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Toutes les cases en * sont lues
    Vivante(std::string filename): Entite(filename){}
    /// Toutes les cases en * sont lues
    Vivante(std::string filename,int x_min,int y_min): Entite(filename,x_min,y_min){}
    /// Vivante vide
    Vivante():Entite(){}
    /// Renvoie le type de l'Entite
    virtual std::string type() const {return "VIVANTE";}

//<<<<<<<<<<<<<<<<<<<< VIVANTE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Indique si elle doit se reproduire, par défaut jamais
    virtual bool should_greed(const Map&ext, const std::map<int,Entite*>& corr) const{return false;}
    /// Renvoie les enfants et les place sur la Map \ext, par défaut liste vide
    virtual std::list<Vivante*> get_children(Map&ext, const std::map<int,Entite*>& corr) const {return std::list<Vivante*>();}
};

#endif
