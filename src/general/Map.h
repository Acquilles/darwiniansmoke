#ifndef MAP_H
#define MAP_H

/*! \file Map.h Définit la classe map et toutes les fonctions de calcul qui nécessitent une représentation en 2D
 */

#include "Case.h"

/*!
 * \class Map
 * \brief Une Map contient un vecteur d'entiers en 2 dimensions et dispose de bcp de fonctions utiles
 */
 class Map
 {
 protected:
//<<<<<<<<<<<<<<<<<<<<<<< ATTRIBUTS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     /// La carte
     std::vector<std::vector<int>> map;
     /// Case du bord inférieur
     Case min;
     /// Case du bord supérieur
     Case max;
/// <<<<<<<<<<<<<<<<<<<<<<< SETTERS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     /// Met la valeur val à la position x,y
     bool set(int x, int y, int val);
     /// Met la valeur val à la position c
     bool set(const Case&c, int val){return set(c.x,c.y,val);}

public:
//<<<<<<<<<<<<<<<<<<<< CONSTRUCTEURS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /** Constructeur qui fixe ses limites avec
    * les min et max de la liste et place chaque case
    * de la liste avec des 1 */
    Map(const std::list<Case>& liste_cases);
    /// Par défaut, carte de taille 1
    Map():min(Case(0,0)),max(Case(0,0)){}
    /// Map avec les bords min et maxy
    Map(const Case&min,const Case&max):min(min),max(max){map = std::vector<std::vector<int>>(max.x-min.x+1,std::vector<int>(max.y-min.y+1,0));}

//<<<<<<<<<<<<<<<<<<< ACCESSEURS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     /// Indique si les coordonnées sont dans la map
     bool inside(int x, int y)const;
     /// Indique si la case est dans la map
     bool inside(const Case&c)const{return inside(c.x,c.y);}
     /// Retourne l'entier correspondant à la position
     int get(int x, int y)const;
     /// Retourne l'entier correspondant à la position
     int get(const Case&c)const{return get(c.x,c.y);}
    /// Renvoie la valeur de la position x,y
    int operator()(int x, int y) const;
    /// Renvoie la valeur de la position c
    int operator()(const Case&c) const;
    /// Retourne une Case aléatoire dans la map
    Case random_inside(int bordure = 0) const{return Case(ale<int>(min.x+ bordure,max.x-bordure),ale<int>(min.y+bordure,max.y-bordure));}
    /// Retourne une Case aléatoire sur la frontière de la map
    Case random_bordure() const;

    /// Le Jeu est friend pour pouvoir lire facilement sa map
    friend class Jeu;
    /// A retirer à l'avenir
    friend class ProjectileGame;
    //<<<<<<<<<<<<<<<<<<<A UTILISER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Ajoute une liste de cases \p de l'id \id à la carte
    bool add_cases(const std::list<Case>& l,int id);
    /// Retire une liste de cases \p de l'id \id de la carte
    bool remove_cases(const std::list<Case>& l,int id);

//<<<<<<<<<<<<<<<<<<<<<<< METHODES DE PARCOURS >>>>>>>>>>>>>>>>>>>>>>>>>
    /// Nombre de positions non nulles dans un rayon rad autour de la Case c
    int nb_case(int rad, const Case&c)const;
    /// Indique s'il y a au moins une case égale à val dans l'entourage de 4 de c
    bool one_case(int val,const Case&c)const;
    /// Frontière intérieure de la map
    std::list<Case> get_in_bounds()const;
    /// Frontière extérieure de la map
    std::list<Case> get_ext_bounds()const;
    /// Transforme la map en string de *
    std::string string_map()const;
    /// Transforme la première liste en * et la deuxième en caractère choisi
    std::string string_map_plus(const std::list<Case>& plus,char car)const;
    /// Complete l'ensemble de Case \p toutes par toutes les Case contiues à la \p c dans la Map
    void contigues(const Case& c, std::set<Case>& toutes);
    /// Retourne les cases contigues en liste
    std::list<Case> contigues(const Case&c);

//<<<<<<<<<<<<<<<<<<<<<<< AFFICHAGE >>>>>>>>>>>>>>>>>>>>>>>>>
    /// Affiche une Map en 2D avec des *
    friend std::ostream& operator<<(std::ostream& os,const Map&map){os << map.string_map();return os;}
    /// Affiche une liste de cases en 2D avec des *
    friend std::ostream& operator<<(std::ostream& os, const std::list<Case>& liste_cases);
 };
//<<<<<<<<<<<<<<<<<<< EXTERIEURES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/// Retourne la liste des listes des cases contigues correspondante
std::list<std::list<Case>> disloque(const std::list<Case>& liste);

#endif
