#ifndef ENTITE_H
#define ENTITE_H
/*!
 * \file Entite.h
 * \brief Définition de la classe Entite
 */

#include "Map.h"
#include "Screen.h"
/*!
 * \class Entite
 * Une entité est composée d'une liste de Case qui variera au cours de la simulation.
 * Chaque tour, la fonction step définit son comportement.
 * \brief Une Entite est composée d'une liste de Case et interragira lors de la simulation
 */
class Entite
{
protected:
//<<<<<<<<<<<<<<<<<<<<<<< ATTRIBUTS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    /// Le compteur d'entités
    static int cpt;
    ///identifiant unique de l'entité
    int _id;
    /// Nombre de steps qui ont été exécutés
    int time;
    /// Liste des Case de l'entité
    std::list<Case> cases;
    /// La Map correspondante à la liste de ses cases
    Map map;

public:
//<<<<<<<<<<<<<<<<<<<< CONSTRUCTEURS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Initialise l'Entite avec un fichier (* si case pleine) avec xmin,ymin = 0,0
    Entite(std::string filename):Entite(filename,0,0){}
    /// Initialise l'Entite avec un fichier (* si case pleine) et les valeurs xmin et ymin
    Entite(std::string filename,int x_min,int y_min);
    /// Entite vide
    Entite():_id(cpt++),time(0),cases(std::list<Case>()){}
    /// Initialise l'Entite en ajoutant la liste de Case à une Map
    Entite(std::list<Case> l, Map&m):Entite(){add_case(l,m);}
    /// Pour bien vérifier leur destruction
    virtual ~Entite(){std::cout << "Destroy " << _id << std::endl;}
//<<<<<<<<<<<<<<<<<<< ACCESSEURS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Nombre de cases de l'Entite
    size_t nb_cases()const{return cases.size();}
    /// Renvoie l'id de  l'Entite
    int id(){return _id;}
    /// Insique si l'Entite est toujours active, par défaut si elle contient encore des cases
    virtual bool active() {return (cases.size()!=0);}
    /// Renvoie toutes les cases détenues par l'entite
    const std::list<Case>& get_cases()const {return cases;}
    /// Renvoie le type de l'Entite, "ENTITE"
    virtual std::string type() const {return "ENTITE";}
    /// Renvoie si l'entite est hostile, false par défaut
    virtual bool hostile()const {return false;}

//<<<<<<<<<<<<<<<<<<< MODIFICATEURS PROTECTED >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
protected:
    /// Ajoute une Case à l'Entite avec ses coordonnées
    virtual void add_case(int x, int y);
    /// Ajoute une Case à l'Entite
    virtual void add_case(const Case& c);
    /// Ajoute une Case à l'Entite avec ses coordonnées
    virtual void add_case(const std::list<Case> &c);

    /// Suprime une liste de Case de l'Entite
    virtual void remove_case(const Case&c);
    /// Suprime une Case de l'Entite
    virtual void remove_case(const std::list<Case> &l);

    /// Met à jour sa carte 2D à l'aide des sa liste de cases et de ses bordures
    virtual void set_map();

//<<<<<<<<<<<<<<<<<<< MODIFICATEURS PUBLIC >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
public:
    /// A UTILISER : Suprime une Case de l'Entite et d'une Map extérieure
    virtual void remove_case(const std::list<Case> &l,Map&ext);
    /// A UTILISER : Ajoute une liste de Case à l'entite et une Map extérieure
    virtual void add_case(const std::list<Case> &c,Map&ext);
    /// A UTILISER : Enlève toutes les cases de l'Entite, sur une carte extéreure
    virtual void clear_it(Map&ext);
    /// Ecrit les statistiques instantanées dans un flux de fichier, par défaut id,time
    virtual void write_stats(std::ofstream&os)const {}

//<<<<<<<<<<<<<<<<<<<<<<<<< AFFICHAGE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Convertit la map en un string comme la disposition de ses cases marqués par une *
    virtual std::string to_string() const;
    /// Affiche l'entite sur un screen, en une variation de noir dépendante de son _id
    virtual void show(Screen s){s.show_case(cases,1.0,0x000000 + 4*_id);}
    /// Affiche un sur un flux, de même manière que to_string()
    friend std::ostream& operator<<( std::ostream& os, const Entite& e);
//<<<<<<<<<<<<<<<<<<<<<<<<< STEP >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /*! Fait avancer la fumée d'un pas
    * \param m Map sur laquelle interragit l'entite.
    * \param corr Dictionnaire des entiers de la map
    * et des Entite correspondantes
     */
    virtual void step(Map&m, const std::map<int,Entite*>& corr) = 0;

//<<<<<<<<<<<<<<<<<<<<<<<<< FRIENDS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Retourne les indices des entites de type \p type dans la map corr
    friend std::list<int> indices_of_type(std::string type,const std::map<int,Entite*> corr);
    /// Retourne les indices des entites hostiles dans la map corr
    friend std::list<int> indices_hostiles(const std::map<int,Entite*> corr);
    /// Retourne les entites de type \p type dans la map corr
    friend std::list<Entite*> entites_of_type(std::string type,const std::map<int,Entite*> corr);
    /// Retourne les entites hostiles dans la map corr
    friend std::list<Entite*> entites_hostiles(const std::map<int,Entite*> corr);
    /// Distance avec la case de \p e la plus proche de \p c
    friend float dist_min(const Case&c,const Entite*e);
    /// Distance entre les deux cases les plus proches de \p a et de \p b
    friend float dist_min(const Entite*a,const Entite*b);
    // Distance avec la case de la liste des Entite \p l la plus proche de \p c
    friend float dist_min(const Case&c,const std::list<Entite*>&l);
    // Distance avec la case de la liste des Entite \p l la plus proche de \p e
    friend float dist_min(const Entite*e,const std::list<Entite*>&l);
    /// Distance minimale de la case avec une Entite de type type
    friend float dist_min(const Case&c, std::string type, const std::map<int,Entite*> corr);
    /// Distance minimale de l'entite avec une Entite de type type
    friend float dist_min(const Entite*c, std::string type, const std::map<int,Entite*> corr);
    /// Indique si une entite en trouche une autre
    friend bool touch(const Entite*a,const Entite*b);
    /// Indique si une entite en trouche une autre dans la liste passée
    friend bool touch(const Entite*a,const std::list<Entite*>&l);
};

#endif
