#ifndef SCREEN_H_
#define SCREEN_H_

/*!
 * \file Screen.h
 * \brief Classe Screen reprise du TP5-MAIN4-POLYTECH-SORBONNE-2019, et modifiée
 * \author Mme Braustein (initialement)
 */

#include <SDL.h>
#include "Case.h"


/// Rayon d'une case à l'écran
#define RCASE 10

/// \class Screen S'occupe d'affiche des trucs à l'écran
class Screen
{
  public:
//<<<<<<<<<<<<<<<<<<<< CONSTRUCTEURS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Par défaut, taille 300px dans tous les sens
    Screen() : Screen(-300,-300,300,300){}
    /*!
       Constructor of Screen  open a SDL window
       \param x_min x_min of the window (e.g. -400)
       \param y_min y_min of the window (e.g. -400)
       \param x_max x_max of the window (e.g. 400)
       \param y_max y_max of the window (e.g. 400)
     */
    Screen(int x_min, int y_min, int x_max, int y_max) : _w(x_max-x_min), _h(y_max-y_min),x_zero(-x_min),y_zero(-y_min)
    {
      if (SDL_Init(SDL_INIT_VIDEO) < 0)
        std::cerr << "error init sdl " << std::endl;
      _screen = SDL_SetVideoMode(_w, _h, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
      assert(_screen);
    }

    /// Construit un écran vide avec les bordures des cases min et max
    Screen(Case min,Case max):Screen(min.x*RCASE,min.y*RCASE,max.x*RCASE,max.y*RCASE){}
    /// Construit un écran avec les bordures de la liste
    Screen(const std::list<Case>& l)
    {
        int x_min , y_min, x_max, y_max;
        std::tie(x_min , y_min, x_max, y_max) = get_bounds(l);
        _w = (x_max-x_min + 8)*RCASE ;
        _h = (y_max-y_min + 8)*RCASE;
        x_zero = -(x_min-4)*RCASE;
        y_zero = -(y_min-4)*RCASE;
        if (SDL_Init(SDL_INIT_VIDEO) < 0)
          std::cerr << "error init sdl " << std::endl;
        _screen = SDL_SetVideoMode(_w, _h, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
        assert(_screen);
    }

//<<<<<<<<<<<<<<<<<<<<<<< PUBLIC FUNCTIONS >>>>>>>>>>>>>>>>>>>>>>>>>>
     /// @return the width of the screen
    int w() const { return _w; }

     /// @return the height of the screen
    int h() const { return _h; }

    /** Dessine une case avec ses coordonnées
       @param x x-coordinate of the center (in pixels)
       @param y y-coordinate of the center (in pixels)
       @param rad Par quoi le rayon est multiplié
       @param color 0xRRGGBBAA (default: black)
     */
    void show_case(const Case& c,float rad=1.0, Uint32 color = 0x000000)
    {
        _show_case(_screen,c,rad,color);
    }
    /*!
     * \brief Afiche une liste de cases sur un Screen
     */
    void show_case(const std::list<Case>& l, float rad = 1.0, Uint32 color = 0x000000)
    {
        for(const Case& c : l)
        {
            _show_case(_screen,c,rad,color);
        }
    }

    /** draw a disc
       @param x x-coordinate of the center (in pixels)
       @param y y-coordinate of the center (in pixels)
       @param radius (in pixels)
       @param color 0xRRGGBBAA (default: red)
     */
    void disc(int x, int y, int radius, Uint32 color = 0xFF0000)
    {
      _disc(_screen, x, y, radius, color);
    }

    /** draw a filled rectangle
       @param x x-coordinate of the center (in pixels)
       @param y y-coordinate of the center (in pixels)
       @param w width of the rectangle (in pixels)
       @param h height of the rectangle (in pixels)
       @param color 0xRRGGBBAA (default: blue)
     */
    void rect(int x, int y, int w, int h, Uint32 color = 0xFF)
    {
      _rect(_screen, x, y, w, h, color);
    }

    /** draw a line
       @param x0 starting x-coordinate (in pixels)
       @param y0 starting y-coordinate (in pixels)
       @param x1 ending x-coordinate (in pixels)
       @param y1 ending y-coordinate (in pixels)
       @param color 0xRRGGBBAA (default: red)
     */
    void line(int x0, int y0, int x1, int y1, Uint32 color = 0xFF0000)
    {
      _line(_screen, x0, y0, x1, y1, color);
    }

    /** draw axes
       @param color 0xRRGGBBAA (default: black)
     */
    void axes(Uint32 color = 0x000000)
    {
      _line(_screen, x_zero, 0, x_zero, _h, color);
      _line(_screen, 0, y_zero, _w, y_zero, color);
    }

    /**
       Flip the screen: swap buffer1 and buffer2; in effect, update the screen to
       take the drawings into account.
       \return faux si on a quitté
     */
    bool flip()
    {
        SDL_Rect r = {0, 0, Uint16(_w), Uint16(_h)};
        SDL_Flip(_screen);
        SDL_FillRect(_screen, &r, 0xFFFFFF);
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
                return false;
        }
        return true;
    }

    /// Are the coordinates inside the screen ?
    bool inside(int x,int y)
    {
        return (x+x_zero>0 && y+y_zero>0);
    }

    /// Set screen bounds
    void set_bounds(int x_min, int y_min, int x_max, int y_max);

    /// destructors
    ~Screen()
    {
        SDL_FreeSurface(_screen);
    }
  protected:
//<<<<<<<<<<<<<<<<<<<<<<< ATTRIBUTS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    SDL_Surface *_screen;
    int _w, _h;
    int x_zero,y_zero;
//<<<<<<<<<<<<<<<<<<<< PROTECTED FUNCTIONS >>>>>>>>>>>>>>>>>>>>>>>>>>
    void _disc(SDL_Surface *surf,
               int x_center, int y_center, int radius,
               Uint32 color);
    void _rect(SDL_Surface *surf,
               int x_center, int y_center, int w, int h,
               Uint32 color);

    void _line(SDL_Surface *surf,
               int x0, int y0, int x1, int y1,
               Uint32 color);
    void _show_case(SDL_Surface *surf,const Case&c,float rad, Uint32 color);
    void _put_pixel(SDL_Surface*surf,
                    Uint32 color, int x, int y)
    {
      if (x >= surf->w || x < 0 || y >= surf->h || y < 0)
        return;
      Uint32 *bufp = (Uint32 *)surf->pixels + y * surf->pitch / 4 + x;
      *bufp = color;
    }
    void _put_pixel(SDL_Surface*surf, int x, int y,
                    Uint8 r, Uint8 g, Uint8 b)
    {  _put_pixel(surf, SDL_MapRGB(surf->format, r, g, b), x, y); }
    ;
};


#endif
