#include "Case.h"
/*! \file Case.cc Définit les fonctions membres et friend de Case */

//<<<<<<<<<<<<<<<<<<<<<<< OPERATORS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
std::ostream& operator<<(std::ostream& os, const Case & c) {
    os << '(' << c.x << ',' << c.y << ')';
    return os;
}
//<<<<<<<<<<<<<<<<<<<<<<<<< FRIENDS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

//<<<<<<<<<<<<<<<<<<<<<<< Utilisant une Case >>>>>>>>>>>>>>>>>>>>>>>>

std::list<Case> contour4(const Case& c)
{
  std::list<Case> l;
  l.push_back(Case(c.x-1,c.y));
  l.push_back(Case(c.x+1,c.y));
  l.push_back(Case(c.x,c.y-1));
  l.push_back(Case(c.x,c.y+1));
  return l;
}

std::list<Case> contour8(const Case& c)
{
  std::list<Case> l;
  for (int i = -1; i <= 1; i++) {
    for (int j = -1; j <= 1; j++) {
      if(i != 0 || j != 0)
      {
        l.push_back(Case(c.x+i,c.y+j));
      }
    }
  }
  return l;
}


bool is_in_bounds(const Case& c, const Case&min, const Case&max)
{
    return c.x <= max.x && c.x >= min.x && c.y <= max.y && c.y >=min.y;
}

//<<<<<<<<<<<<<<<<<<<<<<< Utilisant une liste >>>>>>>>>>>>>>>>>>>>>>>

std::tuple<int, int, int, int> get_bounds(const std::list<Case>& liste_cases)
{
    if(liste_cases.size()==0)
    {
        return std::make_tuple(0,0,0,0);
    }
    int x_min(liste_cases.front().x), x_max(x_min), y_min(liste_cases.front().y), y_max(y_min);
    /// - Pour chaque Case de la liste, on met à jour les valeurs maximales si nécessaire
    for(const auto& it : liste_cases)
    {
        if(it.x > x_max)
            x_max = it.x;
        if(it.x < x_min)
            x_min = it.x;
        if(it.y > y_max)
            y_max = it.y;
        if(it.y < y_min)
            y_min = it.y;
    }
    return std::make_tuple(x_min,y_min,x_max,y_max);
}

std::map<char,std::list<Case>> file_to_map_corr(std::string filename, const Case& min)
{
    std::ifstream f(filename);
    char c;
    int x(0), y(0);
    std::map<char,std::list<Case>> corr_char;
    /// - Pour chaque caractère du fichier :
    while(f.get(c))
    {

        if(c!='\n')
        {
            /// - . Si ce n'est ni un '\n' ni un ' ' :
            if(c!=' ')
            {
                /// - .. Si c'est la première fois qu'il est rencontré :
                if(corr_char.find(c) == corr_char.end())
                {
                    /// - ...On l'ajoute au dictionnaire
                    corr_char[c] = std::list<Case>();

                }
                /// - .. On ajoute la position au caractère correspondant dans le dictionnaire
                corr_char[c].push_back(Case(x,y)+min);
            }
            /// - . Incrémenter y
            y++;
        }
        /// - . Si c'est un retour à la ligne, x++ et y = 0
        else
        {
            y = 0;
            x++;
        }
    }
    f.close();
    return corr_char;
}
std::list<Case> file_to_list_cases(std::string filename, const Case& min)
{
    std::list<Case> l;
    std::ifstream f(filename);
    char c;
    int x(0), y(0);
    /// - Pour chaque caractère lu :
    while(f.get(c))
    {
        switch(c)
        {
            case ' ':
                break;
            /// - . Si c'est *, alors on ajoute la position correspondante à la liste
            case '*':
                l.push_back(Case(min.x+x,min.y+y));
                break;
            /// - . Si c'est un retour à la ligne on incrémente x et reset y
            case '\n':
                x++;
                y=-1;
                break;
        }
        y++;
    }
    f.close();
    return l;
}


//<<<<<<<<<<<<<<<<<<< EXTERIEURES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

void random_form_rec(size_t nb, const Case&c, std::set<Case>& toutes)
{
    int chances(3),sur(4);
    /// - Si la case n'est pas déjà dans toutes
    if(toutes.find(c)==toutes.end() and toutes.size() < nb)
    {
        /// - . On insère \p c à \p toutes, et pour chaque Case \b a autour de \p c :
        toutes.insert(c);
        for(const auto& a : contour8(c))
        {
            /// - .. 'chances' chances sur 'sur' b de l'ajouter à la listeajouter à la liste
            if(std::rand() % sur >= chances )
            {
                /// - ... On éxécute la fonction sur \b a
                random_form_rec(nb,a,toutes);
            }
        }
    }
}
std::list<Case> random_form(size_t nb, const Case& c)
{
    /// - Obtenir la liste grâce à la fonction récursive random_form_rec
    std::list<Case> l;
    std::set<Case> s;
    while(s.size() < nb)
    {
        s.clear();
        random_form_rec(nb,c,s);
    }
    for (const Case& a : s)
    {
        l.push_back(a);
    }
    return l;
}
