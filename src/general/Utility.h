#ifndef UTILITY_H
#define UTILITY_H
/** \file Utility.h
 * \brief Définit quelques fonctions utiles, essentiellement les random. Importe toutes les bibliothèques */

#include <string>
#include <list>
#include <tuple>
#include <vector>
#include <iterator>
#include <sstream>
#include <fstream>
#include <set>
#include <map>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <cassert>
#include <random>
/// Renvoie vrai avec une probabilité de \f$\frac{chance}{sur}$\f
bool chance(int chance,int sur);

template<typename T>
/// Un T aleatoire entre \p inf et  \p sup
inline T ale(T inf,T sup)
{
    return ((std::rand() % 100)/100.0)*(sup-inf) +inf;
}

/// Renvoie un nombre obtenu par une distribution normale d'espérance mu et de variance sigma
float normal(float mu, float sigma);
#endif
