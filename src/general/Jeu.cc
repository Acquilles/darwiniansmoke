#include "Jeu.h"

/*! \file Jeu.cc Définit quelques méthodes de Jeu*/
//<<<<<<<<<<<<<<<<<<<<<<< METHODES PROTEGEES>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

bool Jeu::add_on_map(const std::list<Case>& liste,int id)
{
    for(const Case& c : liste)
    {
        map.set(c,id);
    }
    return true;
}

bool Jeu::rm_of_map(const std::list<Case>& liste)
{
    for(const Case& c : liste)
    {
        map.set(c,0);
    }
    return true;
}

bool Jeu::add_on_map(Entite*e)
{
    return add_on_map(e->get_cases(),e->id());
}

bool Jeu::rm_of_map(Entite*e)
{
    return rm_of_map(e->get_cases());
}

//<<<<<<<<<<<<<<<<<<<<<<< ----------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//<<<<<<<<<<<<<<<<<<<<<<< METHODES PUBLIQUES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//<<<<<<<<<<<<<<<<<<<<<<< ----------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

//<<<<<<<<<<<<<<<<<<<<<<< AJOUT ET SUPPRESSION >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
bool Jeu::add_vivante(Vivante*v)
{
    if(true)
    {
        vivantes.push_back(v);
        corr[v->id()] = v;
        return add_on_map(v);
    }
    return false;
}
bool Jeu::add_entite(Entite*e)
{
    if(true)
    {
        env.push_back(e);
        corr[e->id()] = e;
        return add_on_map(e);
    }
    return false;
}

bool Jeu::rm_entite(Entite*e)
{
    /// - Si on a pu le supprimer de la carte des correspondances
    if(corr.erase(e->id()) != 0)
    {
        /// - . On l'enlève de la liste des env et de la carte
        env.remove(e);
        rm_of_map(e);
        delete e;
        return true;
    }
    return false;
}

bool Jeu::rm_vivante(Vivante*v)
{
    /// - Si on a pu le supprimer de la carte des correspondances
    if(corr.erase(v->id()) != 0)
    {
        /// - . On l'enlève de la liste des vivantes et de la carte
        vivantes.remove(v);
        rm_of_map(v);
        delete v;
        return true;
    }
    return false;
}

//<<<<<<<<<<<<<<<<<<<<<<< COMPORTEMENT >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
bool Jeu::sim(std::string stats)
{
    /// - Inititalise l'écran et le fichier stats
    begin_stats(stats);

    Screen s(map.min,map.max);
    show(s);
    /// - Tant que l'on ne quitte ou restart pas :
    std::string e("");
    while(e!="q" and e!="r") {
        /// - . step, afficher, écrire les statistiques
        e = std::cin.get();
        step();
        show(s);
        write_stats();
    }
    end_stats();
    return (e=="r");
}
void Jeu::step()
{
    std::cout << "Step " << cpt << std::endl;
    step_all_entites();
    step_all_vivantes();
}

void Jeu::step_all_entites()
{
    std::cout << cpt++ << std::endl;
    /// - Etablir la liste des env à supprimer
    std::list<Entite*> to_remove_e;
    for(const auto& e : env)
    {
        //std::cout << e->get_cases().front() << std::endl;
        if (!e->active())
        {
            to_remove_e.remove(e);
        }
        else
        {
            step_entite(e);
        }
    }

    /// - Les supprimer
    for(const auto& e : to_remove_e)
    {
        rm_entite(e);
    }
}
void Jeu::step_all_vivantes()
{
    /// - Etablir la liste des vivantes à ajouter et supprimer
    std::list<Vivante*> to_remove;
    std::list<Vivante*> to_add;
    for(const auto& v : vivantes)
    {
        if (!v->active())
        {
            to_remove.push_back(v);
        }
        else
        {
            step_entite(v);

            if(v->should_greed(map,corr) and allow_reproduction())
            {
                for(const auto& c : v->get_children(map,corr))
                {
                    to_add.push_back(c);
                }
            }
        }
    }

    ///- Les supprimer
    for(const auto& v : to_remove)
    {
        rm_vivante(v);
    }
    ///- Les ajouter
    for(const auto& v : to_add)
    {
        add_vivante(v);
    }
}

void Jeu::step_entite(Entite*e)
{
    return e->step(map,corr);
}

void Jeu::step_vivante(Vivante*v)
{
    return v->step(map,corr);
}


//<<<<<<<<<<<<<<<<<<<<<<< STATISTIQUES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

void Jeu::begin_stats(std::string filename)
{
    stats = std::ofstream(filename);
}

void Jeu::write_stats()
{
    if(stats)
    {    for(const Vivante*v: vivantes)
        {
            stats << cpt << ',' ;
            v->write_stats(stats);
            stats << std::endl;
        }
    }
}

void Jeu::end_stats()
{
    stats.close();
}

//<<<<<<<<<<<<<<<<<<<<<<< AFFICHAGE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

void Jeu::show(Screen s)const
{
    for(Entite * e: env)
    {
        e->show(s);
    }
    for(Entite * v: vivantes)
    {
        v->show(s);
    }
    s.axes();
    s.flip();
}

std::ostream& operator<<(std::ostream&os,const Jeu& j)
{
    os << j.map.string_map();
    return os;
}

//<<<<<<<<<<<<<<<<<<<<<<< DESTRUCTEUR >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

Jeu::~Jeu()
{
    for(Entite * e: env)
    {
        delete e;
    }
    for(Entite * v: vivantes)
    {
        delete v;
    }
}
