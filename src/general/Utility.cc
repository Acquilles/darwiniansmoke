#include "Utility.h"

bool chance(int chance,int sur)
{
  int al = std::rand() % sur +1;
  return (al > sur - chance);
}

float normal(float mu, float sigma)
{
    std::random_device rd{};
    std::mt19937 gen{rd()};
    std::normal_distribution<> d{mu,sigma};
    for(int n=0; n<20; ++n) {
        d(gen);
    }
    auto a =  d(gen);
    return a;
}
