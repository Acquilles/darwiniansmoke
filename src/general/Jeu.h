#ifndef JEU_H
#define JEU_H

/*! \file Jeu.h Définit la classe Jeu et ses fonctions amies */
#include "Vivante.h"

/*!
 * \class Jeu
 * La classe Jeu s'occupe de contrôler la simulation. Elle contient un ensemble d'Entite et la Map, et s'occupe par la fonction sim()
 * de lancer la simulation. Le Jeu gère alors son fonctionnement et son affichage sur un Screen.
 * \brief La classe Jeu s'occupe de contrôler la simulation.
 */
class Jeu
{
//<<<<<<<<<<<<<<<<<<<<<<< ATTRIBUTS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
protected:
    /// Compteur de steps
    size_t cpt;
    /// La carte des Entite présentes, représentées sous forme d'entiers
    Map map;
    /// Ensemble des entités Vivante du jeu.
    std::list<Vivante*> vivantes;
    /// Ensemble des entités non vivantes du jeu, l'environnement
    std::list<Entite*> env;
    /// La correspondance entre chaque Entite présente dans le Jeu son identifiant entier
    std::map<int,Entite*> corr;
    /// Flux de fichier dans lequel le jeu écrit des statistiques à chaque step
    std::ofstream stats;

//<<<<<<<<<<<<<<<<<<<<<<< CONSTRUCTEUR DESTRUCTEURS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
public:
    /// Constructeur par défaut
    Jeu():cpt(0){}
    /// Constructeur avec limites
    Jeu(const Case& min, const Case& max):cpt(0),map(Map(min,max)){}
    /// Constructeur avec limites
    Jeu(Map&m):cpt(0),map(m){}
    /// Détruit toutes les fumées du jeu aussi !
    ~Jeu();
//<<<<<<<<<<<<<<<<<<<<<<< METHODES PROTEGEES>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
protected:
    /// Tente d'ajouter une liste de Case avec l'id sur la carte
    bool add_on_map(const std::list<Case>& liste,int id);
    /// Tente de vider une liste de Case de la carte
    bool rm_of_map(const std::list<Case>& liste);
    /// Tente d'ajouter une entité sur la carte.
    bool add_on_map(Entite*e);
    /// Tente de retirer une entité de la carte.
    bool rm_of_map(Entite*e);

    /// Fais avancer une entité d'un pas
    virtual void step_entite(Entite*e);
    /// Fais avancer une vivante d'un pas
    virtual void step_vivante(Vivante*v);
    /// Avance toutes les entites d'un pas.
    void step_all_entites();
    /// Avance toutes les vivantes d'un pas.
    void step_all_vivantes();
    /// Avance tout d'un pas.
    void step();

    /// Autoriser la reproduction, par défaut il faut qu'il y ait moins de 10 Fumées Vivantes
    virtual bool allow_reproduction(){return 10 > vivantes.size();}
//<<<<<<<<<<<<<<<<<<<<<<< METHODES PUBLIQUES>>>>>>>>>>>>>>>>>>>>>>>>>>>>
public:
    /// Ajoute une Entite dans le Jeu, si elle n'y est pas déjà
    bool add_entite(Entite*e);
    /// Ajoute une entité Vivante dans le Jeu, si elle n'y est pas déjà
    bool add_vivante(Vivante*v);
    /// Retire une Entite du Jeu, si elle est effectivement là
    bool rm_entite(Entite*e);
    /// Retire une entité Vivante du Jeu, si elle est effectivement là
    bool rm_vivante(Vivante*v);

    /**\brief Lance une simulation retourne vrai si restart
     * \param stats "chemin vers le fichier de statistiques à remplir, par défaut ../stats/stats.csv"*/
    bool sim(std::string stats = "../stats/stats.csv");

//<<<<<<<<<<<<<<<<<<<<<<< STATISTIQUES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Crée un fichier de statistiques et l'ouvre
    virtual void begin_stats(std::string filename) ;
    /// Ecrit les tatistiques de l'instant
    virtual void write_stats() ;
    /// Ferme le fichier des statistiques
    virtual void end_stats() ;

//<<<<<<<<<<<<<<<<<<<<<<< AFFICHAGE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /// Affiche le jeu dans le terminal
    friend std::ostream& operator<<(std::ostream&os,const Jeu& j);
    /// Affiche le Jeu sur un Screen
    virtual void show(Screen s)const;

    // friend class Creator;
};


#endif
