#include "Entite.h"
/*! \file Entite.cc Définit les fonctions membres et friend de Entite */
int Entite::cpt = 1;
//<<<<<<<<<<<<<<<<<<<< CONSTRUCTEURS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Entite::Entite(std::string filename,int x_min,int y_min):_id(cpt++),time(0)
{
    /// - On transforme le fichier en liste de Case et on initialise la Map
    add_case(file_to_list_cases(filename,Case(x_min,y_min)));
}

//<<<<<<<<<<<<<<<<<<< MODIFIER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
void Entite::add_case(int x, int y)
{
    cases.push_back(Case(x,y));
}

void Entite::add_case(const Case &c)
{
    cases.push_back(c);
}
void Entite::add_case(const std::list<Case> &l)
{
    for(const auto c : l)
    {
        cases.push_back(c);
    }
}

void Entite::remove_case(const Case& c)
{
    cases.remove(c);
}

void Entite::remove_case(const std::list<Case> &l)
{
    for(const auto c : l)
    {
        cases.remove(c);
    }
}

//<<<<<<<<<<<<<<<<<<< A UTILISER >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
void Entite::add_case(const std::list<Case> &l,Map&m)
{
    /// - Ajoute les cases à la liste
    add_case(l);
    /// - Tente d'ajouter les cases à la map extérieure
    m.add_cases(l,_id);
    /// - Met à jour sa propre map
    set_map();
}

void Entite::remove_case(const std::list<Case> &l,Map& m)
{

    /// - Retire les cases de la liste
    remove_case(l);
    /// - Tente de retirer les cases de la map extérieure
    m.remove_cases(l,_id);
    /// - Met à jour sa propre map
    set_map();
}

void Entite::set_map(){
    map = Map(cases);
}

void Entite::clear_it(Map&ext)
{
    ext.remove_cases(cases,_id);
    cases.clear();
    map = Map();
}
//<<<<<<<<<<<<<<<<<<<<<<<<< AFFICHAGE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
std::string Entite::to_string() const
{
    return map.string_map();
}

void print_time(Screen s)
{

}

//<<<<<<<<<<<<<<<<<<< FRIENDS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
std::ostream& operator<<( std::ostream& os, const Entite& e)
{
    os << e.to_string();
    return os;
}

std::list<int> indices_of_type(std::string type,const std::map<int,Entite*> corr)
{
    std::list<int> indices_list;
    for(std::map<int,Entite*>::const_iterator it = corr.begin();it!=corr.end();it++)
    {
        if(it->second->type() == type)
        {
            indices_list.push_back(it->first);
        }
    }
    return indices_list;
}

std::list<int> indices_hostiles(const std::map<int,Entite*> corr)
{
    std::list<int> indices_list;
    for(std::map<int,Entite*>::const_iterator it = corr.begin();it!=corr.end();it++)
    {
        if(it->second->hostile())
        {
            indices_list.push_back(it->first);
        }
    }
    return indices_list;
}
std::list<Entite*> entites_of_type(std::string type,const std::map<int,Entite*> corr)
{
    std::list<Entite*> entites_list;
    for(std::map<int,Entite*>::const_iterator it = corr.begin();it!=corr.end();it++)
    {
        if(it->second->type() == type)
        {
            entites_list.push_back(it->second);
        }
    }
    return entites_list;
}

std::list<Entite*> entites_hostiles(const std::map<int,Entite*> corr)
{
    std::list<Entite*> entites_list;
    for(std::map<int,Entite*>::const_iterator it = corr.begin();it!=corr.end();it++)
    {
        if(it->second->hostile())
        {
            entites_list.push_back(it->second);
        }
    }
    return entites_list;
}
float dist_min(const Case&c,const Entite*e)
{
    /// -- Complexité e.cases
    float dist_min = -1.0;
    /// - Pour chaque case de l'Entite : tester si c'est la distance minimale
    for(const Case&ce : e->cases)
    {
        if(dist_min > dist(c,ce) or dist_min == -1.0)
        {
            dist_min = dist(c,ce);
        }
    }
    /// - Renvoyer celle-ci
    return dist_min;
}
float dist_min(const Entite*a,const Entite*b)
{
    /// -- Complexité a.cases*b.cases
    float min = -1.0;
    /// - Pour chaque case de la permière Entite : tester si c'est la distance minimale avec l'autre
    for(const Case&ca : a->cases)
    {
        float d = dist_min(ca,b);
        if(min > d or min == -1.0)
        {
            min = d;
        }
    }
    /// - Renvoyer celle-ci
    return min;
}

float dist_min(const Case&c,const std::list<Entite*>&l)
{
    /// -- Complexité : nombre de cases de la liste
    float min = -1.0;
    /// - Pour chaque Entite la liste \p l : tester si c'est la distance minimale avec la case
    for(const Entite*et : l)
    {
        float d = dist_min(c,et);
        if(min>d or min == -1.0)
        {
            min = d;
        }
    }
    /// - Retourner celle-ci
    return min;
}

float dist_min(const Entite*e,const std::list<Entite*>&l)
{
    /// -- Complexité e.cases*(nombre de cases de la liste)
    float min = -1.0;
    /// - Pour chaque case de la première Entite : tester si c'est la distance minimale avec la liste
    for(const Case&ce : e->cases)
    {
        float d = dist_min(ce,l);
        if(min > d or min==-1.0)
        {
            min = d;
        }
    }
    /// - Renvoyer celle-ci
    return min;
}

float dist_min(const Case&c, std::string type, const std::map<int,Entite*> corr)
{
    return dist_min(c,entites_of_type(type,corr));
}

float dist_min(const Entite*e, std::string type, const std::map<int,Entite*> corr)
{
    return dist_min(e,entites_of_type(type,corr));
}

bool touch(const Entite*a,const Entite*b)
{
    for(const Case&c : a->cases)
    {
        for(const Case&cp : b->cases)
        {
            if(dist(c,cp) == 0.0)
            {
                return true;
            }
        }
    }
    return false;
}

bool touch(const Entite*e,const std::list<Entite*>&l)
{
    for(const Entite*el : l)
    {
        if(touch(e,el))
        {
            return true;
        }
    }
    return false;
}
