#include "ProjectileGame.h"
#include "Standart.h"
/*!
 * \file main.cc Définit le lancement du main
 */

#include <ctime>

/// Lance un StandartGame
void standart(char* rules){
    bool a = true;
    while(a){
        StandartGame g(true,rules);
        a = g.sim();
    }
}

/// Choix du Jeu à lancer
int main(int argc, char* argv[])
{
    srand(time(0));
    if(argc > 1)
    {
      /// - Si on a bien spécifié un type de Jeu:
        if(strcmp(argv[1], "standart") == 0)
        {
          /// - . Si spécifiées, lancement avec les règles indiquées
          if(argc > 2)
          {
            standart(argv[2]);
          }
          /// - . Ou bien lancement avec "rules.csv" par défaut
          else
          {
            char regles_defaut[] = {'r','u','l','e','s','.','c','s','v','\0'};
            standart(regles_defaut);
          }
        }
    }
    else
    {
        std::cout << " Pas de Jeu spécifié. Usage par exemple:\n./smoke standart" << std::endl;
    }
}
