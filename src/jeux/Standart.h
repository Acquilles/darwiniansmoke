#ifndef STANDART_GAME_H
#define STANDART_GAME_H

/** \file Standart.h
 * \brief Définit les règles du jeu standart.
*/

#include "../general/Jeu.h"

#include "../entites/Mur.h"
#include "../entites/Projectile.h"
#include "../entites/Fumee_test.h"

#include "../vivantes/Fumee_esquive.h"
#include "../vivantes/Mystherbe.h"
#include "../vivantes/Esquive_vie.h"

#define BORD 10
#define nb_max_vivantes 10
#define STANDART_PATH "standart/"
//<<<<<<<<<<<<<<<<<<<< StandartGame >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/** \class StandartGame
 * \brief Un Jeu dans lequel il se passe beaucoup de choses */
class StandartGame : public Jeu{
private:
    bool allow_reproduction(){return nb_max_vivantes > vivantes.size();}
public:
    void begin_stats(std::string filename);
    StandartGame(bool a,char* regles);

    void add_projectile(size_t nb,std::string regles);
    void add_mur(size_t nb,std::string regles);
    void add_fumee_esquive(size_t nb,std::string regles);
    void add_fumee_test(size_t nb,std::string regles);
    void add_esquive_vie(size_t nb,std::string regles);
    void add_mystherbe(size_t nb,std::string regles);
};
#endif
