#include "Standart.h"

void StandartGame::begin_stats(std::string filename)
{
    stats = std::ofstream(filename);
    stats <<  "step,id,time,taille,min,max,NB_ADD,NB_RM" << std::endl;
}

StandartGame::StandartGame(bool a,char *regles):Jeu()
{
    int x1,x2,y1,y2;
    std::string path(regles);
    std::ifstream fl(STANDART_PATH+path);
    std::string line("");
    std::string param("");
    std::string file;
    size_t nb;
    while(std::getline(fl,line))
    {
        std::istringstream s(line);
        s >> param;
        if(param=="PROJECTILES")
        {
            s >> nb >> file;
            add_projectile(nb,file);
        }
        if(param=="MUR")
        {
            s >> nb >> file;
            add_mur(nb,file);
        }
        if(param=="TEST")
        {
            s >> nb >> file;
            add_fumee_test(nb,file);
        }
        if(param=="ESQUIVE")
        {
            s >> nb >> file;
            add_fumee_esquive(nb,file);
        }
        if(param=="ESQUIVE_VIE")
        {
            s >> nb >> file;
            add_esquive_vie(nb,file);
        }
        if(param=="MYSTHERBE")
        {
            s >> nb >> file;
            add_mystherbe(nb,file);
        }
        if(param=="MAP_BOUNDS")
        {
            s >> x1>>y1>>x2>>y2;
            map = Map(Case(x1,y1),Case(x2,y2));
        }
    }
}

void StandartGame::add_mystherbe(size_t nb,std::string regles)
{
    size_t size_fumee, t_min, t_max;
    float a1, a2, a3, a4, a5, coef;
    std::ifstream fl(STANDART_PATH+regles);
    if(!fl)
    {
        std::cout << regles << ": Fichier problématique" << std::endl;
         exit(0);
    }
    std::string line("");
    std::string param("");
    while(std::getline(fl,line))
    {
        std::istringstream s(line);
        s >> param;
        if(param=="CONSTANTS")
        {
            s >> size_fumee >> t_min >> t_max >> coef ;
        }
        if(param=="INITIAUX")
        {
            s >> a1 >> a2 >> a3 >> a4 >> a5 ;
        }
    }

    for (size_t i = 0; i < nb; i++)
    {
        Case center(map.random_inside(BORD));
        Mystherbe *m = new Mystherbe({a1,a2,a3,a4,a5}, t_min, t_max, coef);
        m->add_case(random_form(size_fumee,center),map);
        add_vivante(m);
    }
}
void StandartGame::add_fumee_esquive(size_t nb,std::string regles)
{
    size_t size_fumee0, t_min0, t_max0, NB_ADD0, NB_RM0;
    size_t size_fumee1, t_min1, t_max1, NB_ADD1, NB_RM1;
    std::ifstream fl(STANDART_PATH+regles);
    if(!fl)
    {
        std::cout << regles << ": Fichier problématique" << std::endl;
         exit(0);
    }
    std::string line("");
    std::string param("");
    while(std::getline(fl,line))
    {
        std::istringstream s(line);
        s >> param;
        if(param=="F_BORNE_INF")
        {
            s >> size_fumee0 >> t_min0 >> t_max0 >> NB_ADD0 >> NB_RM0 ;
        }
        if(param=="F_BORNE_SUP")
        {
            s >> size_fumee1 >> t_min1 >> t_max1 >> NB_ADD1 >> NB_RM1 ;
        }
    }

    for (size_t i = 0; i < nb; i++)
    {
        Case center(map.random_inside(BORD));
        Fumee_esquive *f = new Fumee_esquive(
            ale<size_t>(t_min0,t_min1),
            ale<size_t>(t_max0,t_max1),
            ale<size_t>(NB_ADD0,NB_ADD1),
            ale<size_t>(NB_RM0,NB_RM1),
            2
        );
        f->add_case(random_form(ale<size_t>(size_fumee0,size_fumee1),center),map);
        add_vivante(f);
    }
}

void StandartGame::add_esquive_vie(size_t nb,std::string regles)
{
    size_t size_fumee0, t_min0, t_max0, NB_ADD0, NB_RM0, pdv0;
    size_t size_fumee1, t_min1, t_max1, NB_ADD1, NB_RM1, pdv1;
    std::ifstream fl(STANDART_PATH+regles);
    if(!fl)
    {
        std::cout << regles << ": Fichier problématique" << std::endl;
         exit(0);
    }
    std::string line("");
    std::string param("");
    while(std::getline(fl,line))
    {
        std::istringstream s(line);
        s >> param;
        if(param=="F_BORNE_INF")
        {
            s >> size_fumee0 >> t_min0 >> t_max0 >> NB_ADD0 >> NB_RM0 >> pdv0 ;
        }
        if(param=="F_BORNE_SUP")
        {
            s >> size_fumee1 >> t_min1 >> t_max1 >> NB_ADD1 >> NB_RM1 >> pdv1;
        }
    }

    for (size_t i = 0; i < nb; i++)
    {
        Case center(map.random_inside(BORD));
        Esquive_vie *f = new Esquive_vie(
            ale<size_t>(t_min0,t_min1),
            ale<size_t>(t_max0,t_max1),
            ale<size_t>(NB_ADD0,NB_ADD1),
            ale<size_t>(NB_RM0,NB_RM1),
            2,
            ale<size_t>(pdv0,pdv1)
        );
        f->add_case(random_form(ale<size_t>(size_fumee0,size_fumee1),center),map);
        add_vivante(f);
    }
}

void StandartGame::add_fumee_test(size_t nb,std::string regles)
{
    size_t size_fumee0, min0, max0, et0, gt1;
    size_t size_fumee1, min1, max1, gt0, et1;

    std::ifstream fl(STANDART_PATH+regles);
    if(!fl)
    {
        std::cout << regles << ": Fichier problématique" << std::endl;
         exit(0);
    }

    std::string line("");
    std::string param("");
    while(std::getline(fl,line))
    {
        std::istringstream s(line);
        s >> param;
        if(param=="F_BORNE_INF")
        {
            s >> size_fumee0 >> min0 >> max0 >> gt0 >> gt1 ;
        }
        if(param=="F_BORNE_SUP")
        {
            s >> size_fumee1 >> min1 >> max1 >> et0 >> et1 ;
        }

    }

    for (size_t i = 0; i < nb; i++)
    {
        Case center(map.random_inside(BORD));
        Fumee_test *f = new Fumee_test(
            map,
            ale<size_t>(gt0,gt1),
            ale<size_t>(et0,et1),
            ale<size_t>(min0,min1),
            ale<size_t>(max0,max1)
        );
        f->add_case(random_form(ale<size_t>(size_fumee0,size_fumee1),center),map);
        add_entite(f);
    }
}
void StandartGame::add_projectile(size_t nb,std::string regles)
{
    // Paramètres principaux
    float min_speed_proj,max_speed_proj;
    size_t size_proj0,size_proj1;
    int xmin , xmax , ymin , ymax ;
    // Ouverture du fichier
    std::ifstream fl(STANDART_PATH+regles);
    if(!fl)
    {
        std::cout << regles << ": Fichier problématique" << std::endl;
         exit(0);
    }
    std::string line("");
    std::string param("");
    while(std::getline(fl,line))
    {
        std::istringstream s(line);
        s >> param;
        if(param=="SIZE_PROJ")
        {
            s >> size_proj0>>size_proj1;
        }
        if(param=="BOUNDS_SPEED_PROJ")
        {
            s >> min_speed_proj >> max_speed_proj;

        }
        if(param=="BOUNDS_PROJ")
        {
            s >> xmin >> ymin >> xmax >> ymax ;
        }
    }
    for (size_t i = 0; i < nb; i++)
    {
        float proj_speed_x = ale<float>(-max_speed_proj,max_speed_proj);
        float proj_speed_y = ale<float>(-max_speed_proj,max_speed_proj);
        Case center(map.random_inside(BORD));
        Projectile *p = new Projectile(center,proj_speed_x,proj_speed_y,Case(xmin,ymin),Case(xmax,ymax));
        p->add_case(random_form(ale<size_t>(size_proj0,size_proj1),center),map);
        add_entite(p);
    }
}

void StandartGame::add_mur(size_t nb,std::string regles)
{
    int xmin , ymin , xmax , ymax ;
    std::ifstream fl(STANDART_PATH+regles);
    if(!fl)
    {
        std::cout << regles << ": Fichier problématique" << std::endl;
         exit(0);
    }
    std::string line("");
    std::string param("");
    while(std::getline(fl,line))
    {
        std::istringstream s(line);
        s >> param;
        if(param=="BOUNDS")
        {
            s >> xmin >> ymin >> xmax >> ymax ;
        }
    }
    add_entite(new Mur(xmin , xmax , ymin , ymax , map));
}
