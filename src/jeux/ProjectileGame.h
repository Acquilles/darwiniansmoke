#ifndef PROJECTILE_GAME_H
#define PROJECTILE_GAME_H

/** \file ProjectileGame.h
 * Définit les classes Projectile
 * et le Jeu ProjectileGame.
 * \brief Définit les règles du jeu projectile.
*/

#include "../general/Jeu.h"
#include "../entites/Mur.h"
#include "../entites/Projectile.h"
#include "../vivantes/Fumee_esquive.h"
#include "../vivantes/Mystherbe.h"

//<<<<<<<<<<<<<<<<<<<< ProjectileGame >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/** \class ProjectileGame
 * \brief Un Jeu dans lequel des Fumee_esquive évitent plusieurs Projectile, et se reproduisent */
class ProjectileGame : public Jeu{
private:
    /// Nombre maximal de Fumee_esquive sur la carte
    size_t nb_max_vivantes;
public:
    /** Inititalise le Jeu avec les Projectile et Fumee_esquive indiqués sur une carte.
     * \param filename Ficher texte dans lequel les 'p' correspondent aux Projectile, et le reste aux Fumee_esquive
     * \param taille Taille de la carte
     * \param max_speed_proj Vitesse maximale d'un projectile
     * \param nb_max_vivantes Nombre maximal de Fumee_esquive sur la carte*/
    ProjectileGame(std::string filename, int taille  , float max_speed_proj, size_t nb_max_vivantes);

    // /** Inititalise le Jeu avec les Projectile et Fumee_esquive aléatoires.
    //  * \param nb_proj Nombre de projectiles
    //  * \param nb_fum Nombre de Fumées esquives
    //  * \param taille Taille de la carte
    //  * \param max_speed_proj Vitesse maximale d'un projectile
    //  * \param nb_max_vivantes Nombre maximal de Fumee_esquive sur la carte*/
    ProjectileGame(std::string filename,int a);
    ProjectileGame(std::string rules,int a, int b);
    /// Renvoie le nombre maximal de Fumee_esquive sur la carte
    bool allow_reproduction(){return nb_max_vivantes > vivantes.size();}
};
#endif
