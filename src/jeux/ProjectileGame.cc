#include "ProjectileGame.h"
//<<<<<<<<<<<<<<<<<<<< ProjectileGame >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

ProjectileGame::ProjectileGame(std::string filename, int taille, float max_speed_proj, size_t nb_max_vivantes) : Jeu(),nb_max_vivantes(nb_max_vivantes)
{
    /// - Construction de la map et du Mur qui l'entoure :
    int x1(-taille),x2(taille),y1(-taille),y2(taille);
    Case min(x1,y1),max(x2,y2);
    map = Map(Case(x1,y1),Case(x2,y2));
    add_entite(new Mur(x1,x2,y1,y2,map));

    std::map<char,std::list<Case>> corr_char = file_to_map_corr(filename,Case(-taille/5,-taille/5));
    /// - Pour chaque caractère associé à une liste de Case :
    for(std::map<char,std::list<Case>> ::iterator it = corr_char.begin();it!=corr_char.end();it++)
    {
        /// - . Si c'est un 'p' :
        if(it->first == 'p')
        {
            /// - .. Pour chaque liste de cases contigues dans la liste initiale :
            for(const std::list<Case>& liste : disloque(it->second))
            {
                /// - ... La vitesse du projectile sera tirée au hasard
                float proj_speed_x = ale<float>(-max_speed_proj,max_speed_proj);
                float proj_speed_y = ale<float>(-max_speed_proj,max_speed_proj);
                /// - ... On crée et on ajoute le projectile correspondant aux cases
                Projectile*p = new Projectile(liste.front(),proj_speed_x,proj_speed_y,min,max);
                p->add_case(liste,map);
                add_entite(p);
            }
        }
        /// - . Sinon, on ajoute la fumée esquive correspondante aux cases
        else
        {
            Fumee_esquive*f = new Fumee_esquive();
            f->add_case(it->second,map);
            add_vivante(f);
        }
    }
}


// ProjectileGame(std::string rules)
// {
//     /// - Construction de la map et du Mur qui l'entoure
//     int x1(-taille),x2(taille),y1(-taille),y2(taille);
//     Case min(x1,y1),max(x2,y2);
//     map = Map(min,max);
//     add_entite(new Mur(x1,x2,y1,y2,map));
//
//     for (size_t i = 0; i < nb_proj; i++)
//     {
//         /// - ... On crée et on ajoute le projectile correspondant aux cases
//                 Projectile*p = new Projectile(random_form(),proj_speed_x,proj_speed_y,min,max);
//                 p->add_case(liste,map);
//                 add_entite(p);
//     }
// }

ProjectileGame::ProjectileGame(std::string rules,int wx)
{
    // Paramètres principaux
    int x1,x2,y1,y2;
    Case min,max;
    float min_speed_proj,max_speed_proj;
    size_t nb_fumee, size_fumee0,size_fumee1,nb_proj,size_proj0,size_proj1;
    size_t t_min0,t_max0,NB_ADD0,NB_RM0,mutateur0;
    size_t t_min1,t_max1,NB_ADD1,NB_RM1,mutateur1;

    // Ouverture du fichier
    std::ifstream fr(rules);
    if(!fr)
    {
        std::cout << "Fichier problématique" << std::endl;
         exit(0);
    }
    std::string line("");
    std::string param("");
    while(std::getline(fr,line))
    {
        std::istringstream s(line);
        s >> param;
        if(param=="MAP_BOUNDS")
        {
            s >> x1>>y1>>x2>>y2;
            map = Map(Case(x1,y1),Case(x2,y2));
            min = Case(x1,y1);
            max = Case(x2,y2);
            add_entite(new Mur(x1,x2,y1,y2,map));
        }
        if(param=="NB_MAX_LIVING")
        {
            s >> nb_max_vivantes;
        }
        if(param=="NB_FUMEE")
        {
            s >> nb_fumee;
        }
        if(param=="F_BORNE_INF")
        {
            s >> size_fumee0 >> t_min0 >> t_max0 >> NB_ADD0 >> NB_RM0 >> mutateur0;
        }
        if(param=="F_BORNE_SUP")
        {
            s >> size_fumee1 >> t_min1 >> t_max1 >> NB_ADD1 >> NB_RM1 >> mutateur1;
        }
        if(param=="NB_PROJ")
        {
            s >> nb_proj;
        }
        if(param=="SIZE_PROJ")
        {
            s >> size_proj0>>size_proj1;
        }
        if(param=="BOUNDS_SPEED_PROJ")
        {
            s >> min_speed_proj>>max_speed_proj;

        }
    }
    for (size_t i = 0; i < nb_proj; i++)
    {
        float proj_speed_x = ale<float>(-max_speed_proj,max_speed_proj);
        float proj_speed_y = ale<float>(-max_speed_proj,max_speed_proj);
        Case center(map.random_inside(3));
        Projectile *p = new Projectile(center,proj_speed_x,proj_speed_y,min,max);
        p->add_case(random_form(ale<size_t>(size_proj0,size_proj1),center),map);
        add_entite(p);
    }
    for (size_t i = 0; i < nb_fumee; i++)
    {
        Case center(map.random_inside(10));
        Fumee_esquive *f = new Fumee_esquive(
            ale<size_t>(t_min0,t_min1),
            ale<size_t>(t_max0,t_max1),
            ale<size_t>(NB_ADD0,NB_ADD1),
            ale<size_t>(NB_RM0,NB_RM1),
            ale<size_t>(mutateur0,mutateur1)
        );
        f->add_case(random_form(ale<size_t>(size_fumee0,size_fumee1),center),map);
        add_vivante(f);
    }
}

ProjectileGame::ProjectileGame(std::string rules, int wx, int wxc)
{
    // Paramètres principaux
    int x1,x2,y1,y2;
    Case min,max;
    float min_speed_proj,max_speed_proj;
    size_t nb_fumee, size_fumee0,size_fumee1,nb_proj,size_proj0,size_proj1;
    // Ouverture du fichier
    std::ifstream fr(rules);
    if(!fr)
    {
        std::cout << "Fichier problématique" << std::endl;
         exit(0);
    }
    std::string line("");
    std::string param("");
    while(std::getline(fr,line))
    {
        std::istringstream s(line);
        s >> param;
        if(param=="MAP_BOUNDS")
        {
            s >> x1>>y1>>x2>>y2;
            map = Map(Case(x1,y1),Case(x2,y2));
            min = Case(x1,y1);
            max = Case(x2,y2);
            add_entite(new Mur(x1,x2,y1,y2,map));
        }
        if(param=="NB_MAX_LIVING")
        {
            s >> nb_max_vivantes;
        }
        if(param=="NB_FUMEE")
        {
            s >> nb_fumee;
        }
        if(param=="F_BORNE_INF")
        {
            s >> size_fumee0;
        }
        if(param=="F_BORNE_SUP")
        {
            s >> size_fumee1;
        }
        if(param=="NB_PROJ")
        {
            s >> nb_proj;
        }
        if(param=="SIZE_PROJ")
        {
            s >> size_proj0>>size_proj1;
        }
        if(param=="BOUNDS_SPEED_PROJ")
        {
            s >> min_speed_proj>>max_speed_proj;

        }
    }
    for (size_t i = 0; i < nb_proj; i++)
    {
        float proj_speed_x = ale<float>(-max_speed_proj,max_speed_proj);
        float proj_speed_y = ale<float>(-max_speed_proj,max_speed_proj);
        Case center(map.random_inside(3));
        Projectile *p = new Projectile(center,proj_speed_x,proj_speed_y,min,max);
        p->add_case(random_form(ale<size_t>(size_proj0,size_proj1),center),map);
        add_entite(p);
    }
    for (size_t i = 0; i < nb_fumee; i++)
    {
        Case center(map.random_inside(10));
        Mystherbe *m = new Mystherbe();
        m->add_case(random_form(ale<size_t>(size_fumee0,size_fumee1),center),map);
        add_vivante(m);
    }
}
