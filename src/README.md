# DarwinianSmoke {#mainpage}
# Schéma résumé
Le Jeu contrôle, sur une même Map, différentes Entite, réparties sur leur liste de Case, et les affiche sur un Screen. Les entites peuvent être Vivante et se reproduire, comme Fumee_esquive par exemple.
## Appartenance

Le Jeu va se charger de gérer et de faire interagir ses Entite, qu'elles soient Vivante ou non.
Les Entite sont constituées d'une liste de Case, et disposent chacun d'un comportement différent. C'est au cours de
leur fonction step() qu'ils vont observer la Map rentrée en paramètre et modifier leur structure, c'est à dire la liste des Case qu'ils contiennent.
Au cours de la simulation sim(), le Jeu fait tour à tour avancer d'un step() chaque entité, puis met à jour la Map.
