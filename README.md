# DarwinianSmoke {#mainpage}
Ibtissam, Achille
## Résumé

Une simulation de fumées vivantes disposant de comportements variables et d'un transmission héréditaire de ceux-ci, qui sont soumis à la sélection naturelle dans un environnement hostile.

## Documentation doxygen
https://acquilles.frama.io/darwiniansmoke/dox/html/index.html